﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Database.Configurations
{
    public class contestjuryConfig
    {
        public void Configure(EntityTypeBuilder<ContestJury> entity)
        {
            entity
                .HasKey(e => e.ContestJuryId);



            // Configure many-to-many between Contest and User using ContestJury
            entity
                .HasOne(cj => cj.User)
                .WithMany(u => u.ContestsJuries)
                .HasForeignKey(cj => cj.UserId);



            entity
                .HasOne(cj => cj.Contest)
                .WithMany(c => c.ContestJuries)
                .HasForeignKey(cj => cj.ContestId);
        }
    }
}

