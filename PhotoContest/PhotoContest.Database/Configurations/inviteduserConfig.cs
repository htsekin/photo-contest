﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Database.Configurations
{
    public class inviteduserConfig
    {
        public void Configure(EntityTypeBuilder<InvitedUser> entity)
        {
            entity
                .HasKey(iu => iu.InvitedUserId);



            // Configure many-to-many between Contest and User using ContestJury
            entity
                .HasOne(iu => iu.User)
                .WithMany(u => u.InvitedUsers)
                .HasForeignKey(cj => cj.UserId);



            entity
                .HasOne(iu => iu.Contest)
                .WithMany(c => c.InvitedUsers)
                .HasForeignKey(cj => cj.ContestId);
        }
    }
}
