﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
