﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class Contest
    {
        [Key]
        public int ContestId { get; set; }

        [Required]
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public DateTime CreateOnDate { get; set; }
        public int TimeLimitPhaseOne { get; set; }
        public int TimeLimitPhaseTwo { get; set; }
        public bool isFinished { get; set; }
        public string ImagePath { get; set; }
        public int ContestTypeId { get; set; }
        public int OrganaizerId { get; set; }
        public ContestType ContestType { get; set; }
        public Category Category { get; set; }
        public Organizer Organaizer { get; set; }
        public ICollection<ContestJury> ContestJuries { get; set; } = new List<ContestJury>();
        public ICollection<InvitedUser> InvitedUsers { get; set; } = new List<InvitedUser>();
        public ICollection<ContestItem> ContestItems { get; set; } = new List<ContestItem>();
    }
}
