﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class ContestItem
    {
        [Key]
        public int ContestItemId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Story { get; set; }
        public double Score { get; set; }
        public string PhotoPath { get; set; }
        public int UserId { get; set; }
        public int ContestId { get; set; }

        public User User { get; set; } 
        public Contest Contest { get; set; }
        public List<Review> Reviews { get; set; }
    }
}
