﻿using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class ContestJury
    {
        [Key]
        public int ContestJuryId { get; set; }
        public int ContestId { get; set; }
        public Contest Contest { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
