﻿using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class ContestType
    {
        [Key]
        public int ContestTypeId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
