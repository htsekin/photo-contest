﻿namespace PhotoContest.Database.Models
{
    public class InvitedUser
    {
        public int InvitedUserId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int ContestId { get; set; }
        public Contest Contest { get; set; }
    }
}
