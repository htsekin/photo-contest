﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PhotoContest.Helpers.Strings;

namespace PhotoContest.Database.Models
{
    public class Organizer
    {
        [Key]
        public int OrganaizerId { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessagesTypes.FirstNameMinLength)]
        [MaxLength(20, ErrorMessage = ErrorMessagesTypes.FirstNameMaxLength)]
        public string FirsName { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessagesTypes.LastNameMinLength)]
        [MaxLength(20, ErrorMessage = ErrorMessagesTypes.LastNameMaxLength)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(8, ErrorMessage = ErrorMessagesTypes.PasswordTooShort)]
        //[MaxLength(20, ErrorMessage = MessagesTypes.PasswordTooLong)]
        public string Password { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
