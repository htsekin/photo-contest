﻿using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class Rank
    {
        [Key]
        public int RankId { get; set; }

        [Required]
        public string Name { get; set; }
        public int FromPoints { get; set; }
        public int ToPoints { get; set; }
    }
}
