﻿using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class Review
    {
        [Key]
        public int ReviewId { get; set; }
        public double Score { get; set; }
        public string Comment { get; set; }
        public int ContestItemId { get; set; }
        public int? UserId { get; set; }
        public int? OrganaizerId { get; set; }
        public bool IsWrongCategory { get; set; }
        public ContestItem ContestItem { get; set; }
        public User User { get; set; }
        public Organizer Organaizer { get; set; }
    }
}
