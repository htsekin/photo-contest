﻿using PhotoContest.Helpers.Strings;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Database.Models
{
    public class User
    {
        public int UserId { get; set; }
        
        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessagesTypes.FirstNameMinLength)]
        [MaxLength(20, ErrorMessage = ErrorMessagesTypes.FirstNameMaxLength)]
        public string FirsName { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessagesTypes.FirstNameMinLength)]
        [MaxLength(20, ErrorMessage = ErrorMessagesTypes.FirstNameMaxLength)]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.RequiredField)]
        [MinLength(8, ErrorMessage = ErrorMessagesTypes.PasswordTooShort)]
        //[MaxLength(20, ErrorMessage = MessagesTypes.PasswordTooLong)]
        public string Password { get; set; }
        public int RankingPoints { get; set; }
        public ICollection<ContestJury> ContestsJuries { get; set; } = new List<ContestJury>();
        public ICollection<InvitedUser> InvitedUsers { get; set; } = new List<InvitedUser>();
        public ICollection<ContestItem> ContestItems { get; set; } = new List<ContestItem>();
    }
}
