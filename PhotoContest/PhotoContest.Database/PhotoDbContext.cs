﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace PhotoContest.Database
{
    public class PhotoDbContext : DbContext
    {
        public PhotoDbContext(DbContextOptions<PhotoDbContext> options)
            : base(options)
        {
        }

        public DbSet<ContestType> ContestTypes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Contest> Contests { get; set; }
        public DbSet<ContestItem> ContestItems { get; set; }
        public DbSet<Organizer> Organaizers { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ContestJury> ContestJuries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<InvitedUser> InvitedUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            SeedData(builder);
        }

        private static void SeedData(ModelBuilder builder)
        {
            // Password hashers
            var passHasherUser = new PasswordHasher<User>();
            var passHasherOrganaizer = new PasswordHasher<Organizer>();

            string projectDir = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\.."));
            var seedFilesPath = @"\PhotoContest.Database\SeedDataFiles\";
            var fullPathToSeedFiles = projectDir + seedFilesPath;
            
            var ranks = JsonConvert.DeserializeObject<List<Rank>>(File.ReadAllText(fullPathToSeedFiles + "rank.json"));
            var categories = JsonConvert.DeserializeObject<List<Category>>(File.ReadAllText(fullPathToSeedFiles + "category.json"));
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(fullPathToSeedFiles + "user.json"));
            var contestTypes = JsonConvert.DeserializeObject<List<ContestType>>(File.ReadAllText(fullPathToSeedFiles + "contesttype.json"));
            var organaizers = JsonConvert.DeserializeObject<List<Organizer>>(File.ReadAllText(fullPathToSeedFiles + "organaizer.json"));
            var contests = JsonConvert.DeserializeObject<List<Contest>>(File.ReadAllText(fullPathToSeedFiles + "contest.json"));
            var contestItems = JsonConvert.DeserializeObject<List<ContestItem>>(File.ReadAllText(fullPathToSeedFiles + "contestitem.json"));
            var reviews = JsonConvert.DeserializeObject<List<Review>>(File.ReadAllText(fullPathToSeedFiles + "review.json"));
            var contestJuries = JsonConvert.DeserializeObject<List<ContestJury>>(File.ReadAllText(fullPathToSeedFiles + "selectedjury.json"));
            var invitedUsers = JsonConvert.DeserializeObject<List<InvitedUser>>(File.ReadAllText(fullPathToSeedFiles + "inviteduser.json"));


            foreach (var item in users)
            {
                item.Password = passHasherUser.HashPassword(item, item.Password);
            }

            foreach (var item in organaizers)
            {
                item.Password = passHasherOrganaizer.HashPassword(item, item.Password);
            }

            builder.Entity<User>().HasData(users);
            builder.Entity<Organizer>().HasData(organaizers);
            builder.Entity<Category>().HasData(categories);
            builder.Entity<Rank>().HasData(ranks);
            builder.Entity<Contest>().HasData(contests);
            builder.Entity<ContestItem>().HasData(contestItems);
            builder.Entity<Review>().HasData(reviews);
            builder.Entity<ContestType>().HasData(contestTypes);
            builder.Entity<ContestJury>().HasData(contestJuries);
            builder.Entity<InvitedUser>().HasData(invitedUsers);
        }
}
}
