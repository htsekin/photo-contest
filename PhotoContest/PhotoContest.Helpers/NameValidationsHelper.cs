﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Helpers
{
    public class NameValidationsHelper
    {
        private static readonly int minLenght = 2;
        private static readonly int maxLenght = 20;

        public static void LenghtNameValidation(string name, string validationMsg)
        {
            if (string.IsNullOrWhiteSpace(name) && name.Length < NameValidationsHelper.minLenght && name.Length > NameValidationsHelper.maxLenght)
                throw new ArgumentException(validationMsg);
        }
    }
}

