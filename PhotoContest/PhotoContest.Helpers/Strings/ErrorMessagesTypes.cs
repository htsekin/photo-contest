﻿using System;

namespace PhotoContest.Helpers.Strings
{
    public static class ErrorMessagesTypes
    {
        // Organizer and User messages
        public const string RequiredField = "This field is required!";
        public const string FirstNameMinLength = "First name is too short!";
        public const string FirstNameMaxLength = "First name is too long!";
        public const string LastNameMinLength = "Last name is too short!";
        public const string LastNameMaxLength = "Last name is too long!";
        public const string MinLenValidation = "The {0} must be at least {2} characters long.";

        // Password

        public const string PasswordTooShort = "Password is too short!";
        public const string PasswordTooLong = "Password is too long!";

        // Contest
        public const string TitleFailValidation = "Title must be between 3 and 40 characters long!";
        public const string PhaseOneValidation = "Phase I must be between 1 and 31 days!";
        public const string PhaseTwoValidation = "Phase II must be between 1 and 24 hours!";
        public const string ImageNameValidation = "Image Name must be between 3 and 40 characters long!";
        public const string ContestTypeValidation = "Contest Type with that name does not exist!";
        public const string ContestDoesNotExist = "Contest with id {0} does not exist!";
        public const string ContestsListIsEmpty = "The contest list is empty!";
        public const string JunkieDoesNotHaveContest = "Junkie does not participate in any contest!";

        // Rank
        public const string RankNameValidation = "Rank name must be between 3 and 40 characters long!";
        public const string RankPointsValidation = "Rank points must start with lower number than second one!";
        public const string RankDoesNotExist = "Rank with that id does not exist!";
        public const string PointsRangeNotValid = "Points in that interval does not exist!";

        // ContestItem
        public const string ImageFormatsNotValid = "Only image in png, jpg/jpeg formats are allowed!";
        public const string ContestItemTitleFailValidation = "Title must be between 3 and 40 characters long!";
        public const string ContestItemStoryFailValidation = "Story must be between 10 and 200 characters long!";
        public const string ContestItemListIsEmpty = "The contest item list is empty!";
        public const string NonExistingContestItem = "The contest item does not exist!";
        public const string UploadImageValidation = "Upload image is required!";

        // Category
        public const string NonExistingCategory = "Cannot find the category!";

        // ContestType
        public const string NonExistingType = "Cannot find the type!";
        public const string NotValidType = "The type is not valid!";


        // Organaizer
        public const string NotExistingUser = "User with that username: {0} does not exist!";
        public const string ExistingUser = "User with username: {0} already exist!";
        public const string FirstNameValidation = "First name must be between 2 and 20 characters long!";
        public const string LastNameValidation = "Last name must be between 2 and 20 characters long!";

        // Review
        public const string JuryAndOrganizerIsNull = "Both jury and organizer cannot be null!";
        public const string JuryAndOrganizerIsNotNull = "Both jury and organizer cannot be assigned!";
        public const string NotExistingReview = "Review with that id {0} does not exist!";
        public const string InvalidScore = "Score must be between one and ten!";
        public const string ReviewsListIsEmpty = "The review list is empty!";

        // User
        public const string UserListIsEmpty = "The user list is empty!";

        // ContestJury
        public const string UserOrContestCannotBeNull = "User or Contest cannot be null!";

    }
}
