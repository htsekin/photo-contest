﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly PhotoDbContext context;

        public CategoryService(PhotoDbContext context)
        {
            this.context = context;
        }

        public async Task<CategoryDTO> CreateCategoryAsync(CategoryDTO newCategory)
        {
            var category = new Category();

            category.Name = newCategory.CategoryName;

            var result = new CategoryDTO(category);

            await this.context.Categories.AddAsync(category);

            await this.context.SaveChangesAsync();

            return result;
        }

        public async Task<bool> DeleteCategoryAsync(int id)
        {
            var category = await this.context.Categories.FirstOrDefaultAsync(c => c.CategoryId == id);

            var deleted = this.context.Categories.Remove(category);

            await this.context.SaveChangesAsync();

            if (deleted == null)
            {
                return false;
            }

            return true;
        }

        public async Task<List<CategoryDTO>> GetAllCategoriesAsync()
        {
            return ListCategoryToListDTO(await this.context.Categories.ToListAsync());
        }

        public async Task<Category> GetCategoryByIdAsync(int id)
        {
            var category = await this.context.Categories.FirstOrDefaultAsync(c => c.CategoryId == id);

            return category;
        }

        public async Task<CategoryDTO> UpdateCategoryAsync(CategoryDTO newCategory, int id)
        {
            var category = await this.context.Categories.FirstOrDefaultAsync(c => c.CategoryId == id);

            if (category == null)
            {
                throw new ArgumentException(ErrorMessagesTypes.NonExistingCategory);
            }

            category.Name = newCategory.CategoryName;

            var result = new CategoryDTO(category);

            await this.context.SaveChangesAsync();

            return result;
        }

        private List<CategoryDTO> ListCategoryToListDTO(List<Category> categories)
        {
            var categoriesDTO = new List<CategoryDTO>();
            foreach (var category in categories)
            {
                categoriesDTO.Add(new CategoryDTO(category));
            }

            return categoriesDTO;
        }
    }
}
