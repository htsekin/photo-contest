﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ContestItemService : IContestItemService
    {
        private readonly PhotoDbContext context;
        private readonly IUserService userService;
        private readonly IContestService contestService;
        private readonly IFileService fileService;
        private readonly IOrganizerService organizerService;
        private readonly IReviewService reviewService;
        private readonly IContestJuryService contestJuryService;

        public ContestItemService(PhotoDbContext context,
            IUserService userService, IContestService contestService,
            IFileService fileService, IOrganizerService organizerService,
            IReviewService reviewService, IContestJuryService contestJuryService)
        {
            this.context = context;
            this.userService = userService;
            this.contestService = contestService;
            this.fileService = fileService;
            this.organizerService = organizerService;
            this.reviewService = reviewService;
            this.contestJuryService = contestJuryService;
        }

        public async Task<ContestItemDTO> CreateContestItemAsync(string username, int contestId, ContestItemDTO newContestItem)
        {
            var user = await this.userService.GetAsync(username);
            
            var contest = await this.context.Contests
                .Include(c => c.ContestType)
                .FirstOrDefaultAsync(c => c.ContestId == contestId); //await this.contestService.GetAsync(contestId);

            if (contest.ContestType.Name.ToLower().Equals("open"))
            {
                await this.userService.UpdatePointsAsync(user, 1);
            }
            else if (contest.ContestType.Name.ToLower().Equals("invitational"))
            {
                await this.userService.UpdatePointsAsync(user, 3);
            }
            
            var contestItem = new ContestItem();

            if (string.IsNullOrWhiteSpace(newContestItem.Title) || newContestItem.Title.Length < 3 && newContestItem.Title.Length > 40)
                throw new ArgumentException(ErrorMessagesTypes.ContestItemTitleFailValidation);

            contestItem.Title = newContestItem.Title;

            if (string.IsNullOrWhiteSpace(newContestItem.Story) || newContestItem.Story.Length < 10 && newContestItem.Story.Length > 200)
                throw new ArgumentException(ErrorMessagesTypes.ContestItemStoryFailValidation);

            contestItem.Story = newContestItem.Story;

            if (newContestItem.Photo != null)
                contestItem.PhotoPath = fileService.Upload(newContestItem.Photo);

            contestItem.User = user;
            contestItem.Contest = contest;

            await this.context.ContestItems.AddAsync(contestItem);
            await this.context.SaveChangesAsync();

            await CreateDefaultReview(contestItem.ContestId, contestItem.ContestItemId);

            contestItem = await this.context.ContestItems
                .Include(ci => ci.Reviews)
                .FirstOrDefaultAsync(ci => ci.ContestItemId == contestItem.ContestItemId);

            return new ContestItemDTO(contestItem);
        }

        public async Task<string> EnrollToContestAsync(string username, int contestId, ContestItemDTO newContestItem)
        {
            var contest = await this.context.Contests.FirstOrDefaultAsync(c => c.ContestId == contestId);

            var contestItem = await this.context.ContestItems.FirstOrDefaultAsync(ci => ci.ContestId == contestId && ci.User.Username.ToLower().Equals(username.ToLower()));

            if (contestItem == null)
            {
                if (contest == null)
                {
                    throw new ArgumentException("Contest does not exist!");
                }

                if (!await this.userService.IsInvitedAsync(username, contestId))
                {
                    var createContestItem = await this.CreateContestItemAsync(username, contestId, newContestItem);

                    return $"Enrolled to contest with id: {contestId}";
                }
                else
                {
                    return $"Cannot enroll to contest with id: {contestId}";
                }
            }
            else
            {
                return $"Cannot upload more than one photo!";
            }
        }

        private async Task CreateDefaultReview(int contestId, int contestItemId)
        {
            var organaizers = await this.organizerService.GetAllAsync();
            foreach (var organaizer in organaizers)
            {
                await this.reviewService.CreateReviewAsync(null, organaizer.OrganaizerId, contestItemId);
            }

            var selectedJuries = await this.contestJuryService.GetAllForContestAsync(contestId);
            foreach (var jury in selectedJuries)
            {
                await this.reviewService.CreateReviewAsync(jury.UserId, null, contestItemId);
            }
        }

        public async Task<List<ContestItemDTO>> GetAllAsync()
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.User)
                .Include(ci => ci.Contest)
                .Include(ci => ci.Reviews)
                .ToListAsync();

            return this.ContestItemToContestItemDTO(contestItems);
        }

        public async Task<List<ContestItemDTO>> GetFinishedPhotosAsync(string username)
        {
            var user = await this.userService.GetAsync(username);

            var contestItems = await this.context.ContestItems
                .Include(ci => ci.Contest)
                .Include(ci => ci.User)
                .Where(c => !string.IsNullOrEmpty(c.PhotoPath))
                .Where(ci => ci.UserId == user.UserId)
                .ToListAsync();

            var finished = contestItems
                .Where(ci => this.contestService.IsInPhase(ci.Contest, "Finished")).ToList();

            return this.ContestItemToContestItemDTO(contestItems);

        }
        public async Task<List<ContestItemDTO>> GetAllContestItemsForContestAsync(int contestId)
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.Contest)
                .Include(ci => ci.User)
                .Include(ci => ci.Reviews)
                    .ThenInclude(r => r.Organaizer)
                .Include(ci => ci.Reviews)
                    .ThenInclude(r => r.User)
                .Where(ci => ci.ContestId == contestId)
                .ToListAsync();

            return this.ContestItemToContestItemDTO(contestItems);
        }

        public async Task<ContestItemDTO> GetContestItemAsync(int contestItemId)
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.Contest)
                .Include(ci => ci.User)
                .Include(ci => ci.Reviews)
                .FirstOrDefaultAsync(ci => ci.ContestItemId == contestItemId);

            return new ContestItemDTO(contestItems);
        }

        public async Task<List<ContestItemDTO>> GetItemForUserAsync(int userId)
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.User)
                .Include(ci => ci.Contest)
                .Include(ci => ci.Reviews)
                .Where(ci => ci.UserId == userId)
                .ToListAsync();

            return this.ContestItemToContestItemDTO(contestItems);
        }
        public async Task<List<ContestItemDTO>> GetItemsForUserInFinishedAsync(int userId)
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.User)
                .Include(ci => ci.Contest)
                .Where(c => c.Contest.isFinished == true)
                .Include(ci => ci.Reviews)
                .Where(ci => ci.UserId == userId)
                .ToListAsync();

            return this.ContestItemToContestItemDTO(contestItems);
        }

        public async Task<bool> IsAbleToWriteReviewAsync(int currentUserId, int contestItemId)
        {
            bool temp = await this.context.ContestItems
                .Include(ci => ci.Reviews)
                .AnyAsync(ci => ci.ContestItemId == contestItemId && ci.Reviews.Any(r => r.UserId == currentUserId));

            return temp;
        }

        public async Task<bool> IsPhotoUploadedAsync(int contestItemId)
        {
            var contestItem = await this.context.ContestItems
                    .FirstOrDefaultAsync(ci => ci.ContestItemId == contestItemId);

            if (contestItem == null)
            {
                throw new ArgumentException(ErrorMessagesTypes.NonExistingContestItem);
            }

            if (string.IsNullOrEmpty(contestItem.PhotoPath))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public async Task CalcAvarage(List<ContestItemDTO> contestItems)
        {
            foreach (var item in contestItems)
            {
                var avg = 0d;
                if (item.Reviews.Any())
                {
                    avg = item.Reviews.Select(r => r.Score).Average();
                    item.Score = avg;

                    var contestItem = await this.context.ContestItems.FirstOrDefaultAsync(ci => ci.ContestItemId == item.ContestItemId);
                    contestItem.Score = item.Score;
                }
            }
            await this.context.SaveChangesAsync();
        }

        public async Task<List<ContestItemDTO>> GetWinners(List<ContestItemDTO> contestItems, ContestDTO contest)
        {
            var winners = this.GetWinnersFromList(contestItems);
            var uniqueScores = new Dictionary<double, int>();

            if(this.contestService.GetContestPhase(contest) == "Finished" && contest.IsFinished == false)
            {
                await this.contestService.UpdateContestStatus(contest.ContestId);

                foreach (var item in winners)
                {
                    if (uniqueScores.ContainsKey(item.Score))
                    {
                        uniqueScores[item.Score]++;
                    }
                    else
                    {
                        uniqueScores.Add(item.Score, 1);
                    }
                }

                var cnt1 = 1;
                var cnt2 = 1;
                var dictAsList = uniqueScores.Keys.ToList();

                foreach (var scoreScore in uniqueScores)
                {
                    foreach (var item in winners)
                    {
                        if(scoreScore.Key == item.Score && scoreScore.Value == 1)
                        {
                            if (cnt1 == 1)
                            {
                                if((scoreScore.Key / 2) > dictAsList[1])
                                {
                                    await this.userService.UpdatePointsAsync(item.User, 75);
                                    item.RankingPoints += 75;
                                    break;
                                }
                                await this.userService.UpdatePointsAsync(item.User, 50);
                                item.RankingPoints += 50;
                                break;
                            }
                            else if (cnt1 == 2)
                            {
                                await this.userService.UpdatePointsAsync(item.User, 35);
                                item.RankingPoints += 35;
                                break;
                            }
                            else if (cnt1 == 3)
                            {
                                await this.userService.UpdatePointsAsync(item.User, 20);
                                item.RankingPoints += 20;
                                break;
                            }
                        }

                        if (scoreScore.Key == item.Score && scoreScore.Value > 1)
                        {
                            if (cnt2 == 1)
                            {
                                await this.userService.UpdatePointsAsync(item.User, 40);
                                item.RankingPoints += 40;
                            }
                            else if (cnt2 == 2)
                            {
                                await this.userService.UpdatePointsAsync(item.User, 25);
                                item.RankingPoints += 25;
                            }
                            else if (cnt2 == 3)
                            {
                                await this.userService.UpdatePointsAsync(item.User, 10);
                                item.RankingPoints += 10;
                            }
                        }

                    }
                    cnt1++;
                    cnt2++;
                }
            }

            return winners;
        }

        private List<ContestItemDTO> GetWinnersFromList(List<ContestItemDTO> contestItems)
        {
            var rankDict = contestItems.OrderByDescending(ci => ci.Score).ToList();

            List<ContestItemDTO> top3 = new List<ContestItemDTO>();
            HashSet<double> uniques = new HashSet<double>();
            foreach (var contestItem in rankDict)
            {
                if(uniques.Count < 3)
                    uniques.Add(contestItem.Score);
                if (uniques.Contains(contestItem.Score))
                {
                    top3.Add(contestItem);
                }
                
            }

            return top3;
        }

        public async Task<List<ContestItemDTO>> GetTopTen()
        {
            var contestItems = await this.context.ContestItems
                .Include(ci => ci.Contest)
                .Include(ci => ci.User)
                .Include(ci => ci.Reviews)
                .Where(ci => ci.Score != 0)
                .ToListAsync();

            var result = contestItems.GroupBy(ci => ci.ContestId)
                     .SelectMany(g => g.OrderByDescending(ci => ci.Score).Take(1));

            var topTen = result.Where(ci => this.contestService.IsInPhase(ci.Contest, "Finished"))
                .Take(10).OrderByDescending(ci => ci.Score).ToList();

            return ContestItemToContestItemDTO(topTen);
        }

        private List<ContestItemDTO> ContestItemToContestItemDTO(List<ContestItem> contestItems)
        {
            var contestItemsDTO = new List<ContestItemDTO>();

            foreach (var item in contestItems)
            {
                contestItemsDTO.Add(new ContestItemDTO(item));
            }
            return contestItemsDTO;
        }

        

        
    }
}

