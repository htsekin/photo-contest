﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ContestJuryService : IContestJuryService
    {
        private readonly PhotoDbContext context;

        public ContestJuryService(PhotoDbContext context)
        {
            this.context = context;
        }

        public async Task<ContestJury> GetAsync(string username)
        {
            if (username == null)
            {
                return null;
            }

            var result = await this.context.ContestJuries
                .Include(cj => cj.User)
                .Include(cj => cj.Contest)
                .FirstOrDefaultAsync(u => u.User.Username.ToLower().Equals(username.ToLower()));

            //if (result == null)
            //    throw new ArgumentNullException(string.Format(ErrorMessagesTypes.NotExistingUser, username));

            return result;
        }
        public async Task<List<ContestJury>> GetAllForContestAsync(int contestId)
        {
            var result = await this.context.ContestJuries
                .Include(r => r.User)
                .Include(r => r.Contest)
                .Where(r => r.ContestId == contestId)
                .ToListAsync();

            //if (result.Count == 0)
            //    throw new ArgumentNullException(ErrorMessagesTypes.ContestDoesNotExist);

            return result;
        }

        public async Task<ContestJury> CreateJuryAsync(int userId, int contestId)
        {
            var jury = new ContestJury();

            jury.UserId = userId;
            jury.ContestId = contestId;

            await this.context.ContestJuries.AddAsync(jury);
            await this.context.SaveChangesAsync();

            return jury;
        }
    }
}
