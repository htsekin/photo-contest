﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PhotoContest.Helpers.Strings;
using System.Linq;

namespace PhotoContest.Services
{
    public class ContestService : IContestService
    {
        private readonly PhotoDbContext context;
        private readonly IFileService fileService;
        private readonly IOrganizerService organizerService;
        private readonly IContestJuryService contestJuryService;
        private readonly ICategoryService categoryService;
        private readonly IInvitedUsers invitedUsers;

        public ContestService(PhotoDbContext context, IContestTypeService typeService,
            IFileService fileService,
            IOrganizerService organizerService, IContestJuryService contestJuryService,
            ICategoryService categoryService, IInvitedUsers invitedUsers)
        {
            this.context = context;
            this.fileService = fileService;
            this.organizerService = organizerService;
            this.contestJuryService = contestJuryService;
            this.categoryService = categoryService;
            this.invitedUsers = invitedUsers;
        }

        public async Task<List<ContestDTO>> GetAllAsync()
        {
            var result = new List<ContestDTO>();
            var contests = await context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .Include(c => c.ContestItems)
                    .ThenInclude(ci => ci.User)
                .ToListAsync();

            foreach (var item in contests)
            {
                result.Add(new ContestDTO(item));
            }

            return result;
        }

        public async Task<ContestDTO> CreateContestAsync(ContestDTO model, int contestCreatorId, List<int> selectedJuries)
        {
            var newContest = new Contest();

            if (model.Title.Length < 2 || model.Title.Length > 40)
                throw new ArgumentException(ErrorMessagesTypes.TitleFailValidation);

            newContest.Title = model.Title;

            if (model.TimeLimitPhaseOne < 1 || model.TimeLimitPhaseOne > 31)
                throw new ArgumentException(ErrorMessagesTypes.PhaseOneValidation);

            newContest.TimeLimitPhaseOne = model.TimeLimitPhaseOne;

            if (model.TimeLimitPhaseTwo < 1 || model.TimeLimitPhaseTwo > 24)
                throw new ArgumentException(ErrorMessagesTypes.PhaseTwoValidation);

            newContest.TimeLimitPhaseTwo = model.TimeLimitPhaseTwo;
            newContest.ContestType = await this.context.ContestTypes.FirstOrDefaultAsync(c => c.ContestTypeId == int.Parse(model.ContestType));
            newContest.CreateOnDate = model.CreateOnDate;
            newContest.OrganaizerId = contestCreatorId;
            newContest.Category = await this.categoryService.GetCategoryByIdAsync(int.Parse(model.Category));

            if (model.File != null)
            {
                newContest.ImagePath = fileService.Upload(model.File);

            }
            else
            {
                newContest.ImagePath = "contest-default.jpg";
            }


            await context.Contests.AddAsync(newContest);
            await context.SaveChangesAsync();

            // ToDo implement in separate method
            foreach (var item in selectedJuries)
            {
                await this.contestJuryService.CreateJuryAsync(item, newContest.ContestId);
            }

            // ToDo implement in separate method
            if (newContest.ContestType.Name.ToLower().Equals("invitational"))
            {
                foreach (var item in model.Participants)
                {
                    await this.invitedUsers.CreateInviteAsync(item, newContest.ContestId);
                }
            }

            var result = await this.GetAsync(newContest.ContestId);

            return result;
        }

        public async Task<bool> UpdateContestStatus(int contestId)
        {
            var result = await this.context.Contests.FirstOrDefaultAsync(c => c.ContestId == contestId);
            result.isFinished = true;

            this.context.Contests.Update(result);
            await this.context.SaveChangesAsync();

            if (result == null)
                return false;
            
            return true;
        }

        public async Task<ContestDTO> GetAsync(int id)
        {
            var result = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.ContestJuries)
                .Include(c => c.InvitedUsers)
                .Include(c => c.ContestType)
                .Include(c => c.Organaizer)
                .FirstOrDefaultAsync(c => c.ContestId == id);

            if (result == null)
                throw new ArgumentNullException(string.Format(ErrorMessagesTypes.ContestDoesNotExist, id));

            return new ContestDTO(result);
        }
      
        public async Task<List<ContestDTO>> GetContestsThatIsInJury(int userId)
        {
            var contests = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .Where(c => c.ContestJuries.Any(cj => cj.UserId == userId))
                .ToListAsync();
            
            return this.ContestsToContestsDTO(contests);
        }

        public bool IsInPhase(Contest contest, string contestPhase)
        {
            return GetContestPhase(contest).ToLower().Equals(contestPhase.ToLower());
        }

        public bool IsInvited(int contestId, User user)
        {
            return user.InvitedUsers.Any(c => c.ContestId == contestId);
        }

        public string GetContestPhase(Contest contest)
        {
            return getCntPhase(contest.CreateOnDate, contest.TimeLimitPhaseOne, contest.TimeLimitPhaseTwo);
        }

        public string GetContestPhase(ContestDTO contest)
        {
            return getCntPhase(contest.CreateOnDate, contest.TimeLimitPhaseOne, contest.TimeLimitPhaseTwo);
        }

        private string getCntPhase(DateTime creteOn, int phaseOneDuration, int phaseTwoDuration)
        {
            

            if (DateTime.UtcNow < creteOn)
            {
                return "Not Started";
            }
            else if (DateTime.UtcNow > creteOn && DateTime.UtcNow < creteOn.AddDays(phaseOneDuration))
            {
                return $"Phase I";
            }
            else
            {
                if (DateTime.UtcNow > creteOn.AddDays(phaseOneDuration) && DateTime.UtcNow < creteOn.AddDays(phaseOneDuration).AddHours(phaseTwoDuration))
                {
                    return $"Phase II";
                }
                else
                {
                    return "Finished";
                }
            }
        }

        public async Task<List<ContestDTO>> GetJunkieContestsByPhaseAsync(int junkieId, string contestPhase)
        {
            var contests = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .Where(c => c.ContestItems.Any(ci => ci.UserId == junkieId))
                .ToListAsync();

            var contestsDTO = new List<ContestDTO>();

            foreach (var item in contests)
            {
                if (GetContestPhase(item).ToLower().Equals(contestPhase.ToLower()))
                    contestsDTO.Add(new ContestDTO(item));
            }

            return contestsDTO;
        }

        public async Task<List<ContestDTO>> GetJunkieContestsAsync(int junkieId)
        {
            var contests = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .Where(c => c.ContestItems.Any(ci => ci.UserId == junkieId))
                .ToListAsync();
            
            var notFinished = contests.Where(c => this.IsInPhase(c, "Phase I") == true && this.IsInPhase(c, "Phase II") == true).ToList();

            return this.ContestsToContestsDTO(notFinished);
        }


        public async Task<List<ContestDTO>> GetAllOpenContestsAsync()
        {
            var contests = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .Where(c => c.ContestType.Name.ToLower().Equals("open"))
                .ToListAsync();

            var notFinished = contests.Where(c => this.IsInPhase(c, "Finished") == false && this.IsInPhase(c, "Phase II") == false).ToList();

            return this.ContestsToContestsDTO(notFinished);
        }

        public async Task<List<ContestDTO>> GetContestsByPhaseAsync(string contestPhase)
        {
            var contests = await this.context.Contests
                .Include(c => c.Category)
                .Include(c => c.Organaizer)
                .Include(c => c.ContestType)
                .ToListAsync();

            var contestsDTO = new List<ContestDTO>();

            foreach (var item in contests)
            {
                if (GetContestPhase(item).ToLower().Equals(contestPhase.ToLower()))
                    contestsDTO.Add(new ContestDTO(item));
            }

            return contestsDTO;
        }

        public TimeSpan CountDownPhaseOneTimeSpan(DateTime startDate, int length)
        {
            var passDays = startDate.Subtract(DateTime.UtcNow);
            var phaseOneTimeSpan = TimeSpan.FromDays(length);
            var timeToEnd = phaseOneTimeSpan - (TimeSpan.FromDays(0) - passDays);
            return timeToEnd;
        }

        public TimeSpan CountDownPhaseTwoTimeSpan(DateTime startDate, int lengthPhase1, int lengthPhase2)
        {
            var passHours = startDate.AddDays(lengthPhase1).Subtract(DateTime.UtcNow);
            var phaseTwoTimeSpan = TimeSpan.FromHours(lengthPhase2);
            var timeToEnd = phaseTwoTimeSpan - (TimeSpan.FromDays(0) - passHours);
            return timeToEnd;
        }

        public async Task<string> GetTimeToNextPhaseAsync(int contestId)
        {
            var contest = await this.context.Contests
                .FirstOrDefaultAsync(c => c.ContestId == contestId);

            if (contest == null)
                throw new ArgumentNullException(string.Format(ErrorMessagesTypes.ContestDoesNotExist, contestId));

            var phaseOneSpan = TimeSpan.FromDays(contest.TimeLimitPhaseOne);
            var phaseTwoSpan = TimeSpan.FromHours(contest.TimeLimitPhaseTwo);
            
            if (DateTime.UtcNow < contest.CreateOnDate)
            {
                return "Not Started";
            }
            else if (DateTime.UtcNow > contest.CreateOnDate && DateTime.UtcNow < contest.CreateOnDate.AddDays(contest.TimeLimitPhaseOne))
            {
                var tmpOne = CountDownPhaseOneTimeSpan(contest.CreateOnDate, contest.TimeLimitPhaseOne);
                return $"{this.GetContestPhase(contest)}, Time Left: {tmpOne.ToString(@"d' days 'hh':'mm':'ss")}";
            }
            else
            {
                if (DateTime.UtcNow > contest.CreateOnDate.AddDays(contest.TimeLimitPhaseOne) && DateTime.UtcNow < contest.CreateOnDate.AddDays(contest.TimeLimitPhaseOne).AddHours(contest.TimeLimitPhaseTwo))
                {
                    var tmpTwo = CountDownPhaseTwoTimeSpan(contest.CreateOnDate, contest.TimeLimitPhaseOne, contest.TimeLimitPhaseTwo);
                    return $"{this.GetContestPhase(contest)}, Time Left: {tmpTwo.ToString(@"hh\:mm\:ss")}";
                }
                else
                {
                    return "Finished";
                }
            }
        }

        public async Task<bool> IsContestJury(int contestId, string username)
        {
            //var contest = await this.context.Contests
            //    .Include(c => c.InvitedUsers)
            //        .ThenInclude(iu => iu.User)
            //    .FirstOrDefaultAsync(c => c.ContestId == contestId);

            var juries = await this.contestJuryService.GetAllForContestAsync(contestId);

            var jury = await this.contestJuryService.GetAsync(username);

            if (juries.Contains(jury))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsContestOrganizerJury(string username)
        {
            var organizer = await this.organizerService.GetByUsernameAsync(username);

            if (organizer != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private List<ContestDTO> ContestsToContestsDTO(List<Contest> contests)
        {
            var contestsDTO = new List<ContestDTO>();

            foreach (var item in contests)
            {
                contestsDTO.Add(new ContestDTO(item));
            }
            return contestsDTO;
        }

        public async Task<string> GetContestCategoryAsync(int contesId)
        {
            var contest = await this.GetAsync(contesId);
            return contest.Category;
        }

        public async Task<string> GetContestTypeAsync(int contesId)
        {
            var contest = await this.GetAsync(contesId);
            return contest.ContestType;
        }
    }
}
