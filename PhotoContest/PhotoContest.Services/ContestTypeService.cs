﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ContestTypeService : IContestTypeService
    {
        private readonly PhotoDbContext context;
        public ContestTypeService(PhotoDbContext context)
        {
            this.context = context;
        }

        public async Task<bool> IsValidType(string typeName)
        {
            return await this.context.ContestTypes.AnyAsync(ct => ct.Name.ToLower().Equals(typeName));
        }
        public async Task<ContestTypeDTO> CreateContestTypeAsync(ContestTypeDTO newContestType)
        {
            var contestType = new ContestType();

            contestType.Name = newContestType.ContestTypeName;

            var result = new ContestTypeDTO(contestType);

            await this.context.ContestTypes.AddAsync(contestType);

            await this.context.SaveChangesAsync();

            return result;
        }

        public async Task<bool> DeleteContestTypeAsync(int id)
        {
            var contestType = await this.context.ContestTypes.FirstOrDefaultAsync(c => c.ContestTypeId == id);

            var deleted = this.context.ContestTypes.Remove(contestType);

            await this.context.SaveChangesAsync();

            if (deleted == null)
            {
                return false;
            }

            return true;
        }

        public async Task<List<ContestTypeDTO>> GetAllAsync()
        {
            var result = new List<ContestTypeDTO>();
            var contests = await context.ContestTypes
                .ToListAsync();

            foreach (var item in contests)
            {
                result.Add(new ContestTypeDTO(item));
            }

            return result;
        }

        public async Task<ContestTypeDTO> GetAsync(int id)
        {
            var result = await context.ContestTypes
                .FirstOrDefaultAsync(c => c.ContestTypeId == id);

            return new ContestTypeDTO(result);
        }

        public async Task<bool> IsValidTypeAsync(string contestTypeName)
        {
            var isExist = await this.context.ContestTypes
                .AnyAsync(ct => ct.Name.ToLower().Equals(contestTypeName.ToLower()));
            return isExist;
        }
        public async Task<ContestTypeDTO> UpdateContestTypeAsync(ContestTypeDTO newContestType, int id)
        {
            var contestType = await this.context.ContestTypes.FirstOrDefaultAsync(c => c.ContestTypeId == id);

            if (contestType == null)
            {
                throw new ArgumentException(ErrorMessagesTypes.NonExistingType);
            }

            contestType.Name = newContestType.ContestTypeName;

            var result = new ContestTypeDTO(contestType);

            await this.context.SaveChangesAsync();

            return result;
        }
    }
}
