﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface ICategoryService
    {
        Task<Category> GetCategoryByIdAsync(int id);
        Task<List<CategoryDTO>> GetAllCategoriesAsync();
        Task<CategoryDTO> CreateCategoryAsync(CategoryDTO newCategory);
        Task<CategoryDTO> UpdateCategoryAsync(CategoryDTO newCategory, int id);
        Task<bool> DeleteCategoryAsync(int id);

    }
}
