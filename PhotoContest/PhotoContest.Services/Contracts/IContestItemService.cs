﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestItemService
    {
        Task<ContestItemDTO> CreateContestItemAsync(string username, int contestId, ContestItemDTO contestItem);
        public Task<string> EnrollToContestAsync(string username, int contestId, ContestItemDTO newContestItem);
        Task<List<ContestItemDTO>> GetAllAsync();
        Task CalcAvarage(List<ContestItemDTO> contestItems);
        Task<List<ContestItemDTO>> GetAllContestItemsForContestAsync(int contestId);
        Task<ContestItemDTO> GetContestItemAsync(int contestItemId);
        public Task<List<ContestItemDTO>> GetItemsForUserInFinishedAsync(int userId);
        Task<bool> IsPhotoUploadedAsync(int contestItemId);
        Task<List<ContestItemDTO>> GetFinishedPhotosAsync(string username);
        Task<List<ContestItemDTO>> GetTopTen();
        Task<List<ContestItemDTO>> GetWinners(List<ContestItemDTO> contestItems, ContestDTO contest);
        Task<bool> IsAbleToWriteReviewAsync(int currentUserId, int contestItemId);

        Task<List<ContestItemDTO>> GetItemForUserAsync(int userId);

    }
}
