﻿using PhotoContest.Database.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestJuryService
    {
        Task<List<ContestJury>> GetAllForContestAsync(int contestId);
        Task<ContestJury> CreateJuryAsync(int userId, int contestId);
        //Task<ContestJury> CreateJuryAsync(User user, Contest contest);
        public Task<ContestJury> GetAsync(string username);
    }
}
