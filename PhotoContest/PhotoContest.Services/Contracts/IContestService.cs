﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestService
    {
        Task<ContestDTO> GetAsync(int id);
        Task<List<ContestDTO>> GetAllAsync();
        Task<bool> UpdateContestStatus(int contestId);
        Task<List<ContestDTO>> GetContestsThatIsInJury(int userId);
        string GetContestPhase(Contest contest);
        string GetContestPhase(ContestDTO contest);
        Task<string> GetContestCategoryAsync(int contesId);
        Task<string> GetContestTypeAsync(int contesId);
        Task<List<ContestDTO>> GetContestsByPhaseAsync(string contestPhase);
        Task<List<ContestDTO>> GetJunkieContestsByPhaseAsync(int junkieId, string contestPhase);
        Task<List<ContestDTO>> GetJunkieContestsAsync(int junkieId);
        Task<List<ContestDTO>> GetAllOpenContestsAsync();
        public Task<bool> IsContestJury(int contestId, string username);
        public Task<bool> IsContestOrganizerJury(string username);
        TimeSpan CountDownPhaseTwoTimeSpan(DateTime startDate, int lengthPhase1, int lengthPhase2);
        TimeSpan CountDownPhaseOneTimeSpan(DateTime startDate, int length);
        Task<string> GetTimeToNextPhaseAsync(int contestId);
        bool IsInPhase(Contest contest, string contestPhase);
        bool IsInvited(int contestId, User user);
        Task<ContestDTO> CreateContestAsync(ContestDTO model, int contestCreatorId, List<int> selectedJuries);
    }
}
