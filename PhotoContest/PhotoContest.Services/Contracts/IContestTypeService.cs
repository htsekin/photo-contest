﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestTypeService
    {
        Task<bool> IsValidType(string typeName);
        Task<ContestTypeDTO> GetAsync(int id);
        Task<List<ContestTypeDTO>> GetAllAsync();
        Task<bool> IsValidTypeAsync(string contestTypeName);
        Task<ContestTypeDTO> CreateContestTypeAsync(ContestTypeDTO newContestType);
        Task<ContestTypeDTO> UpdateContestTypeAsync(ContestTypeDTO newContestType, int id);
        Task<bool> DeleteContestTypeAsync(int id);
    }
}
