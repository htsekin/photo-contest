﻿using Microsoft.AspNetCore.Http;

namespace PhotoContest.Services.Contracts
{
    public interface IFileService
    {
        string Upload(IFormFile file);
    }
}
