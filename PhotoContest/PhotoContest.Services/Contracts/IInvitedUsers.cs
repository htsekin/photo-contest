﻿using PhotoContest.Database.Models;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IInvitedUsers
    {
        Task<InvitedUser> CreateInviteAsync(int userId, int contestId);
    }
}
