﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IOrganizerService
    {
        Task<List<Organizer>> GetAllAsync();
        Task<Organizer> GetByUsernameAsync(string username);
        Task<OrganaizerDTO> CreateAsync(OrganaizerDTO model, int organizerId);
        Task<OrganaizerDTO> UpdateAsync(OrganaizerDTO model, string username);
    }
}
