﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IRankService
    {
        Task<RankDTO> GetAsync(int id);
        Task<List<RankDTO>> GetAllAsync();
        Task<RankDTO> Create(RankDTO rank);
        Task<RankDTO> Update(int id, RankDTO rank);
        bool Delete(int id);
        Task<string> GetJunkieRankByPointsAsync(int junkiePoints);
        Task<int> PointsToNextRankAsync(int junkiePoints);
    }
}
