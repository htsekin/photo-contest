﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IReviewService
    {
        Task<List<ReviewDTO>> GetAllReviewsAsync();
        Task<ReviewDTO> GetSpecificReviewAsync(int contestItemId, int junkieId);
        Task<List<ReviewDTO>> GetAllCommentsForItemAsync(int contestItemId);
        Task<List<ReviewDTO>> GetAllReviewsForContestItemAsync(int contestItemId);
        Task<Review> GetUserReviewAsync(int contestItemId, int userId);
        Task<Review> GetOrganizerReviewAsync(int contestItemId, int organizerId);
        Task<ReviewDTO> CreateReviewAsync(int? juryId, int? ogranizerId, int contestItemId);
        Task<ReviewDTO> UpdateReviewAsync(ReviewDTO newReview, string userUsername, string organizerUsername, int contestItemId);
    }
}
