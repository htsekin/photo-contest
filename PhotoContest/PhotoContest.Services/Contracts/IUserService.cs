﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IUserService
    {
        Task<int> GetUserPointsAsync(string username);
        Task<bool> IsEnrolledAsync(string username, int contestId);
        bool IsEnrolled(User user, int contestId);
        Task<bool> IsInvitedAsync(string username, int contestId);
        Task<List<UserDTO>> OrderUsersByRatingAsync();
        Task<string> GetRankingInformationAsync(string username);
        Task<UserDTO> UpdatePointsAsync(User user, int updatePoints);
        Task<List<User>> GetUsersThatCanBeJuryAsync();
        Task<List<User>> GetUsersThatCanBeParticipantsAsync();
        Task<List<UserDTO>> GetAllAsync();
        Task<List<UserDTO>> GetAllContestParticipantsAsync(int contestId);
        Task<User> GetAsync(string username);
        Task<UserDTO> CreateUserAsync(UserDTO model);
        Task<RegisterDTO> CreateUserAsync(RegisterDTO model);
    }
}
