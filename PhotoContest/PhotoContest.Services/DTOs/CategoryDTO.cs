﻿using PhotoContest.Database.Models;

namespace PhotoContest.Services.DTOs
{
    public class CategoryDTO
    {
        public CategoryDTO(Category category)
        {
            this.CategoryId = category.CategoryId;
            this.CategoryName = category.Name;
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
