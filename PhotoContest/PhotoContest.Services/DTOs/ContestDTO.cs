﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;

namespace PhotoContest.Services.DTOs
{
    public class ContestDTO
    {
        public ContestDTO() { }
        public ContestDTO(Contest contest)
        {
            this.ContestId = contest.ContestId;
            this.Title = contest.Title;
            this.Category = contest.Category.Name;
            this.ContestType = contest.ContestType.Name;
            this.CreateOnDate = contest.CreateOnDate;
            this.TimeLimitPhaseOne = contest.TimeLimitPhaseOne;
            this.TimeLimitPhaseTwo = contest.TimeLimitPhaseTwo;
            this.PhotoName = contest.ImagePath;

            foreach (var item in contest.ContestJuries)
            {
                this.JuriesIds.Add(item.UserId);
            }
            this.IsFinished = contest.isFinished;
        }
        public int ContestId { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string ContestType { get; set; }
        public bool IsFinished { get; set; }
        public DateTime CreateOnDate { get; set; }
        public int TimeLimitPhaseOne { get; set; }
        public int TimeLimitPhaseTwo { get; set; }
        public IFormFile File { get; set; }
        public string PhotoName { get; set; }
        public List<int> Participants { get; set; } = new List<int>();
        public List<int> JuriesIds { get; set; } = new List<int>();
    }
}
