﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
        public class ContestItemDTO
        {
            public ContestItemDTO() { }

            public ContestItemDTO(ContestItem contestItem)
            {
                this.ContestItemId = contestItem.ContestItemId;
                this.ContestId = contestItem.ContestId;
                this.Name = contestItem.User.FirsName + " " + contestItem.User.LastName;
                this.Title = contestItem.Title;
                this.Story = contestItem.Story;
                this.Score = contestItem.Score;
                this.RankingPoints = contestItem.User.RankingPoints;
                this.User = contestItem.User;
                this.PhotoName = contestItem.PhotoPath;
                
                foreach (var item in contestItem.Reviews)
                {
                    this.Reviews.Add(new ReviewDTO(item));
                }
                
        }

            public int ContestItemId { get; set; }
            public int ContestId { get; set; }
            public string Name { get; set; }
            public string Title { get; set; }
            public string Story { get; set; }
            public double Score { get; set; }
            public string PhotoName { get; set; }
            public IFormFile Photo { get; set; }
            public List<ReviewDTO> Reviews { get; set; } = new List<ReviewDTO>();
            public int RankingPoints { get; set; }
            public User User { get; set; }
    }
 }

