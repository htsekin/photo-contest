﻿using PhotoContest.Database.Models;

namespace PhotoContest.Services.DTOs
{
    public class ContestTypeDTO
    {
        public ContestTypeDTO() {}
        public ContestTypeDTO(ContestType contestType)
        {
            this.ContestTypeId = contestType.ContestTypeId;
            this.ContestTypeName = contestType.Name;
        }

        public int ContestTypeId { get; set; }
        public string ContestTypeName { get; set; }
    }
}
