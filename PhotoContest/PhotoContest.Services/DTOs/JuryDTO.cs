﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class JuryDTO
    {
        string Username { get; set; }
        public string Role { get; set; }
    }
}
