﻿using PhotoContest.Database.Models;

namespace PhotoContest.Services.DTOs
{
    public class OrganaizerDTO
    {
        public OrganaizerDTO() { }

        public OrganaizerDTO(Organizer organaizer)
        {
            this.FirsName = organaizer.FirsName;
            this.LastName = organaizer.LastName;
            this.Username = organaizer.Username;
            this.Password = organaizer.Password;
        }
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
