﻿using PhotoContest.Database.Models;

namespace PhotoContest.Services.DTOs
{
    public class RankDTO
    {
        public RankDTO(){ }
        public RankDTO(Rank rank)
        {
            this.Name = rank.Name;
            this.FromPoints = rank.FromPoints;
            this.ToPoints = rank.ToPoints;
        }
        public string Name { get; set; }
        public int FromPoints { get; set; }
        public int ToPoints { get; set; }
    }
}
