﻿using PhotoContest.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class RegisterDTO
    {
        public RegisterDTO()
        {

        }
        public RegisterDTO(User user)
        {
            this.FirsName = user.FirsName;
            this.LastName = user.LastName;
            this.Username = user.Username;
            this.Password = user.Password;

        }
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
