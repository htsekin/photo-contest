﻿using PhotoContest.Database.Models;

namespace PhotoContest.Services.DTOs
{
    public class ReviewDTO
    {
        public ReviewDTO()
        {

        }
        public ReviewDTO(Review review)
        {
            this.ReviewId = review.ReviewId;
            this.Score = review.Score;
            this.IsWrongCategory = review.IsWrongCategory;
            this.Comment = review.Comment;
            this.ContestItemId = review.ContestItemId;
            this.UserId = review.UserId;
            this.OrganaizerId = review.OrganaizerId;
            this.UserName = review.User != null ? review.User.FirsName + " " + review.User.LastName : null;
            this.OrganizerName = review.Organaizer != null ? review.Organaizer.FirsName + " " + review.Organaizer.LastName : null;
        }
        public int ReviewId { get; set; }
        public double Score { get; set; }
        public string Comment { get; set; }
        public int ContestItemId { get; set; }
        public int? UserId { get; set; }
        public int? OrganaizerId { get; set; }
        public bool IsWrongCategory { get; set; }
        public string UserName { get; set; }
        public string OrganizerName { get; set; }

    }
}
