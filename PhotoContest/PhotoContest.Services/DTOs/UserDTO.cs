﻿using PhotoContest.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace PhotoContest.Services.DTOs
{
    public class UserDTO
    {
        public UserDTO()
        {

        }
        public UserDTO(User user)
        {
            this.UserId = user.UserId;
            this.FirsName = user.FirsName;
            this.LastName = user.LastName;
            this.Username = user.Username;
            this.Password = user.Password;
            this.RankingPoints = user.RankingPoints;
            this.InvitedUsers = user.InvitedUsers.ToList();
            this.ContestItems = ContestItemToContestItemDTO(user.ContestItems.ToList());
        }
        public int UserId { get; set; }
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RankingPoints { get; set; }
        public List<InvitedUser> InvitedUsers { get; set; } = new List<InvitedUser>();
        public List<ContestItemDTO> ContestItems { get; set; } = new List<ContestItemDTO>();

        private List<ContestItemDTO> ContestItemToContestItemDTO(List<ContestItem> contestItems)
        {
            var contestItemsDTO = new List<ContestItemDTO>();

            foreach (var item in contestItems)
            {
                contestItemsDTO.Add(new ContestItemDTO(item));
            }
            return contestItemsDTO;
        }
    }
}
