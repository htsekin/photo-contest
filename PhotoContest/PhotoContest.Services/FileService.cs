﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Services.Contracts;
using System.IO;
using System;
using Microsoft.AspNetCore.Hosting;

namespace PhotoContest.Services
{
    public class FileService : IFileService
    {
        private readonly IWebHostEnvironment enviroment;

        public FileService(IWebHostEnvironment env)
        {
            this.enviroment = env;
        }

        public string Upload(IFormFile file)
        {
            var uploadDir = "uploads/";
            var uploadPath = Path.Combine(enviroment.WebRootPath, uploadDir);

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            var fileName = Guid.NewGuid() + "___" + file.FileName; // + Path.GetExtension(file.FileName);
            var filePath = Path.Combine(uploadPath, fileName);

            using (var strem = File.Create(filePath))
            {
                file.CopyTo(strem);
            }
            return fileName;
        }
    }
}
