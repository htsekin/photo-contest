﻿using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class InvitedUsers : IInvitedUsers
    {
        private readonly PhotoDbContext context;

        public InvitedUsers(PhotoDbContext context)
        {
            this.context = context;
        }
        public async Task<InvitedUser> CreateInviteAsync(int userId, int contestId)
        {
            var invite = new InvitedUser();

            invite.UserId = userId;
            invite.ContestId = contestId;

            await this.context.InvitedUsers.AddAsync(invite);
            await this.context.SaveChangesAsync();

            return invite;
        }
    }
}
