﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class OrganaizerService : IOrganizerService
    {
        private readonly PhotoDbContext context;
        private readonly PasswordHasher<Organizer> passHasherOrganaizer = new PasswordHasher<Organizer>();

        public OrganaizerService(PhotoDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Organizer>> GetAllAsync()
        {
            var result = await this.context.Organaizers.ToListAsync();
           
            return result;
        }

        public async Task<Organizer> GetByUsernameAsync(string username)
        {
            if (username == null)
            {
                return null;
            }

            var result = await this.context.Organaizers
                .FirstOrDefaultAsync(o => o.Username.ToLower().Equals(username.ToLower()));

            //if (result == null)
            //    throw new ArgumentNullException(ErrorMessagesTypes.NotExistingUser);

            return result;
        }
        public async Task<OrganaizerDTO> CreateAsync(OrganaizerDTO model, int organizerId)
        {
            var newOrganizer = new Organizer();

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.FirstNameValidation);
            newOrganizer.FirsName = model.FirsName;

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.LastNameValidation);
            newOrganizer.LastName = model.LastName;

            newOrganizer.Password = passHasherOrganaizer.HashPassword(newOrganizer, model.Password);

            if (!await this.context.Organaizers.AnyAsync(o => o.Username.ToLower().Equals(model.Username.ToLower())))
            {
                newOrganizer.Username = model.Username;
                await this.context.Organaizers.AddAsync(newOrganizer);
                await this.context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException(ErrorMessagesTypes.ExistingUser);
            }

            var tmp = new OrganaizerDTO(newOrganizer);
            tmp.Password = null;

            return tmp;
        }

        public async Task<OrganaizerDTO> UpdateAsync(OrganaizerDTO model, string username)
        {
            var organizer = await this.context.Organaizers.FirstOrDefaultAsync(o => o.Username.ToLower().Equals(username.ToLower()));

            if (!string.IsNullOrEmpty(model.Password))
            {
                var newPassHashed = passHasherOrganaizer.HashPassword(organizer, model.Password);

                if (!organizer.Password.Equals(newPassHashed))
                    organizer.Password = newPassHashed;
            }

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.FirstNameValidation);
            organizer.FirsName = model.FirsName ?? organizer.FirsName;

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.LastNameValidation);
            organizer.LastName = model.LastName ?? organizer.LastName;

            await this.context.SaveChangesAsync();

            var tmp = new OrganaizerDTO(organizer);
            tmp.Password = null;

            return tmp;
        }
    }
}
