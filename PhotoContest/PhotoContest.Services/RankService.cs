﻿using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using PhotoContest.Helpers.Strings;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace PhotoContest.Services
{
    public class RankService : IRankService
    {
        private readonly PhotoDbContext context;
        public RankService(PhotoDbContext context)
        {
            this.context = context;
        }

        public async Task<List<RankDTO>> GetAllAsync()
        {
            var result = await this.context.Ranks.ToListAsync();
            var ranksDTO = new List<RankDTO>();

            foreach (var item in result)
            {
                ranksDTO.Add(new RankDTO(item));
            }
            return ranksDTO;
        }

        public async Task<RankDTO> GetAsync(int id)
        {
            var result = await this.context.Ranks
                .FirstOrDefaultAsync(r => r.RankId == id);

            if(result == null)
                throw new ArgumentNullException(ErrorMessagesTypes.RankDoesNotExist);

            return new RankDTO(result);
        }

        public async Task<RankDTO> Create(RankDTO rank)
        {
            if (string.IsNullOrWhiteSpace(rank.Name) && rank.Name.Length < 3 && rank.Name.Length > 40)
                throw new ArgumentException(ErrorMessagesTypes.RankNameValidation);

            if (rank.FromPoints > rank.ToPoints)
                throw new ArgumentException(ErrorMessagesTypes.RankPointsValidation);

            var newRank = new Rank();
            newRank.Name = rank.Name;
            newRank.FromPoints = rank.FromPoints;
            newRank.ToPoints = rank.ToPoints;

            await this.context.Ranks.AddAsync(newRank);
            await this.context.SaveChangesAsync();

            return rank;
        }

        public bool Delete(int id)
        {
            var toBeDeleted = this.context.Ranks.FirstOrDefault(r => r.RankId == id);
            if(toBeDeleted != null)
            {
                this.context.Ranks.Remove(toBeDeleted);
                return true;
            }

            return false;
        }

        public async Task<RankDTO> Update(int id, RankDTO rank)
        {
            var result = await this.context.Ranks
                .FirstOrDefaultAsync(r => r.RankId == id);

            if (result == null)
                throw new ArgumentNullException(ErrorMessagesTypes.RankDoesNotExist);

            result.Name = rank.Name ?? result.Name;

            result.FromPoints = rank.FromPoints;
            result.ToPoints = rank.ToPoints;

            return new RankDTO(result);
        }

        public async Task<string> GetJunkieRankByPointsAsync(int junkiePoints)
        {
            var result = await this.context.Ranks.ToListAsync();
            foreach (var item in result)
            {
                if (junkiePoints >= item.FromPoints && junkiePoints <= item.ToPoints)
                {
                    return item.Name;
                }   
            }

            throw new ArgumentNullException(ErrorMessagesTypes.PointsRangeNotValid);
        }

        public async Task<int> PointsToNextRankAsync(int junkiePoints)
        {
            int pointsToNextRank = 0;

            var ranks = await this.context.Ranks.ToListAsync();

            for (int i = 0; i < ranks.Count(); i++)
            {
                if (ranks[i].FromPoints < junkiePoints && ranks[i].ToPoints > junkiePoints)
                {
                    pointsToNextRank = (ranks[i].ToPoints - junkiePoints) + 1;
                }
            }

            return pointsToNextRank;
        }
    }
}
