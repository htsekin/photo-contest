﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ReviewService : IReviewService
    {
        private readonly PhotoDbContext context;
        private readonly IUserService userService;
        private readonly IOrganizerService organizerService;

        public ReviewService(PhotoDbContext context, IUserService userService,
            IOrganizerService organizerService)
        {
            this.context = context;
            this.userService = userService;
            this.organizerService = organizerService;
        }

        public async Task<ReviewDTO> CreateReviewAsync(int? juryId, int? organizerId, int contestItemId)
        {
            var review = new Review();

            review.Score = 3;
            review.IsWrongCategory = false;

            if (organizerId == null && juryId == null)
            {
                throw new ArgumentNullException(ErrorMessagesTypes.JuryAndOrganizerIsNull);
            }
            if (organizerId != null && juryId != null)
            {
                throw new ArgumentException(ErrorMessagesTypes.JuryAndOrganizerIsNotNull);
            }

            if (!await this.context.ContestItems.AnyAsync(ci => ci.ContestItemId == contestItemId))
            {
                throw new ArgumentException("Contest item does not exist!");
            }

            review.OrganaizerId = organizerId;
            review.UserId = juryId;
            review.ContestItemId = contestItemId;

            var result = new ReviewDTO(review);

            await this.context.Reviews.AddAsync(review);
            await this.context.SaveChangesAsync();

            return result;
        }

        public async Task<ReviewDTO> UpdateReviewAsync(ReviewDTO newReview, string userUsername, string organizerUsername, int contestItemId)
        {
            
            var review = new Review();

            if(userUsername != null)
            {
                var jury = await this.userService.GetAsync(userUsername);
                review = await this.GetUserReviewAsync(contestItemId, jury.UserId);


            }
            else if(organizerUsername != null)
            {
                var organizer = await this.organizerService.GetByUsernameAsync(organizerUsername);
                review = await this.GetOrganizerReviewAsync(contestItemId, organizer.OrganaizerId);
            }

            if (review == null)
                throw new ArgumentNullException(ErrorMessagesTypes.NotExistingReview);


            if (newReview.IsWrongCategory == true)
            {
                review.Score = 0;
            }
            else if (newReview.Score < 1 && newReview.Score > 10)
            {
                throw new ArgumentException(ErrorMessagesTypes.InvalidScore);
            }
            else
            {
                review.Score = newReview.Score;
            }

            review.Comment = newReview.Comment;

            var result = new ReviewDTO(review);

            this.context.Reviews.Update(review);
            await this.context.SaveChangesAsync();

            return result;
        }

        public async Task<List<ReviewDTO>> GetAllCommentsForItemAsync(int contestItemId)
        {
            var reviews = await this.context.Reviews
                .Where(r => r.ContestItemId == contestItemId)
                .Include(r => r.Organaizer)
                .Include(r => r.User)
                .Include(r => r.ContestItem)
                    .ThenInclude(ci => ci.Contest)
                .ToListAsync();

            if (reviews == null)
            {
                throw new ArgumentNullException(ErrorMessagesTypes.NotExistingReview);
            }

            return ReviewModelToDTO(reviews);
        }

        public async Task<List<ReviewDTO>> GetAllReviewsAsync()
        {
            var reviews = await this.context.Reviews.ToListAsync();

            if (reviews.Count == 0)
            {
                throw new ArgumentException(ErrorMessagesTypes.ReviewsListIsEmpty);
            }

            return ReviewModelToDTO(reviews);
        }
        public async Task<List<ReviewDTO>> GetAllReviewsForContestItemAsync(int contestItemId)
        {
            var reviews = await this.context.Reviews
                .Include(r => r.Organaizer)
                .Include(r => r.User)
                .Include(r => r.ContestItem)
                    .ThenInclude(ci => ci.Contest)
                .Where(r => r.ContestItemId == contestItemId)
                .ToListAsync();

            return ReviewModelToDTO(reviews);
        }

        public async Task<Review> GetUserReviewAsync(int contestItemId, int userId)
        {
            var review = await this.context.Reviews
                .Include(r => r.Organaizer)
                .Include(r => r.User)
                .Include(r => r.ContestItem)
                    .ThenInclude(ci => ci.Contest)
                .FirstOrDefaultAsync(r => r.ContestItemId == contestItemId && r.UserId == userId);

            return review;
        }

        public async Task<Review> GetOrganizerReviewAsync(int contestItemId, int organizerId)
        {
            var review = await this.context.Reviews
                .Include(r => r.Organaizer)
                .Include(r => r.User)
                .Include(r => r.ContestItem)
                    .ThenInclude(ci => ci.Contest)
                .FirstOrDefaultAsync(r => r.ContestItemId == contestItemId && r.OrganaizerId == organizerId);

            return review;
        }


        public async Task<ReviewDTO> GetSpecificReviewAsync(int contestItemId, int userId)
        {
            var review = await this.context.Reviews
                .Include(r => r.Organaizer)
                .Include(r => r.User)
                .Include(r => r.ContestItem)
                .FirstOrDefaultAsync(r => r.ContestItemId == contestItemId && r.UserId == userId);

            if (review == null)
            {
                throw new ArgumentNullException(ErrorMessagesTypes.NotExistingReview);
            }

            var result = new ReviewDTO(review);

            return result;
        }
       
        private List<ReviewDTO> ReviewModelToDTO(List<Review> reviews)
        {
            var result = new List<ReviewDTO>();

            foreach (var item in reviews)
            {
                result.Add(new ReviewDTO(item));
            }

            return result;
        }
    }
}
