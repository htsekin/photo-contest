﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Helpers;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class UserService : IUserService
    {
        private readonly PhotoDbContext context;
        private readonly IRankService rankService;
        private readonly IContestService contestService;
        private readonly PasswordHasher<User> passHasherOrganaizer = new PasswordHasher<User>();

        public UserService(PhotoDbContext context, IRankService rankService, IContestService contestService)
        {
            this.context = context;
            this.rankService = rankService;
            this.contestService = contestService;
        }

        public async Task<UserDTO> CreateUserAsync(UserDTO model)
        {
            var newUser = new User();

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.FirstNameValidation);
            newUser.FirsName = model.FirsName;

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.LastNameValidation);
            newUser.LastName = model.LastName;

            newUser.Password = passHasherOrganaizer.HashPassword(newUser, model.Password);

            var tmpUser = await this.context.Users.FirstOrDefaultAsync(u => u.Username.ToLower().Equals(model.Username.ToLower()));
            
            if (tmpUser == null)
            {
                newUser.Username = model.Username;
                await this.context.Users.AddAsync(newUser);
                await this.context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException(string.Format(ErrorMessagesTypes.ExistingUser, tmpUser.Username));
            }

            var tmp = new UserDTO(newUser);
            tmp.Password = null;

            return tmp;
        }

        public async Task<RegisterDTO> CreateUserAsync(RegisterDTO model)
        {
            var newUser = new User();

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.FirstNameValidation);
            newUser.FirsName = model.FirsName;

            NameValidationsHelper.LenghtNameValidation(model.FirsName, ErrorMessagesTypes.LastNameValidation);
            newUser.LastName = model.LastName;

            newUser.Password = passHasherOrganaizer.HashPassword(newUser, model.Password);

            var tmpUser = await this.context.Users.FirstOrDefaultAsync(u => u.Username.ToLower().Equals(model.Username.ToLower()));

            if (tmpUser == null)
            {
                newUser.Username = model.Username;
                await this.context.Users.AddAsync(newUser);
                await this.context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException(string.Format(ErrorMessagesTypes.ExistingUser, tmpUser.Username));
            }

            var tmp = new RegisterDTO(newUser);
            tmp.Password = null;

            return tmp;
        }

        public async Task<List<UserDTO>> GetAllAsync()
        {
            var result = await this.context.Users
                .Include(u => u.ContestItems)
                    .ThenInclude(ci => ci.Reviews)
                .ToListAsync();

            if (result.Count == 0)
            {
                throw new ArgumentException(ErrorMessagesTypes.UserListIsEmpty);
            }

            return UserListToUserDTOList(result);
        }

        public async Task<List<UserDTO>> GetAllContestParticipantsAsync(int contestId)
        {
            var result = await this.context.Users
                .Include(u => u.ContestItems)
                .Where(u => u.ContestItems.Any(ci => ci.ContestId == contestId))
                .ToListAsync();


            if (result.Count == 0)
            {
                throw new ArgumentException(ErrorMessagesTypes.UserListIsEmpty);
            }

            return UserListToUserDTOList(result);
        }

        public async Task<User> GetAsync(string username)
        {
            var result = await this.context.Users
                .Include(u => u.ContestItems)
                    .ThenInclude(ci => ci.Reviews)
                .Include(u => u.InvitedUsers)
                    .ThenInclude(iu => iu.Contest)
                    .ThenInclude(c => c.Category)
                .Include(u => u.InvitedUsers)
                    .ThenInclude(iu => iu.Contest)
                    .ThenInclude(c => c.ContestType)
                .Include(u => u.ContestsJuries)
                    .ThenInclude(cj => cj.Contest)
                    .ThenInclude(c => c.Category)
                .Include(u => u.ContestsJuries)
                    .ThenInclude(cj => cj.Contest)
                    .ThenInclude(c => c.ContestType)
                .Include(u => u.ContestItems)
                    .ThenInclude(cj => cj.Contest)
                    .ThenInclude(c => c.Category)
                .Include(u => u.ContestItems)
                    .ThenInclude(ci => ci.Contest)
                    .ThenInclude(c => c.ContestType)
                .FirstOrDefaultAsync(u => u.Username.ToLower().Equals(username.ToLower()));

            //if (result == null)
            //    throw new ArgumentNullException(string.Format(ErrorMessagesTypes.NotExistingUser, username));

            return result;
        }

        public async Task<int> GetUserPointsAsync(string username)
        {
            var result = await this.context.Users
                .FirstOrDefaultAsync(u => u.Username.ToLower().Equals(username.ToLower()));

            if (result == null)
                throw new ArgumentNullException(string.Format(ErrorMessagesTypes.NotExistingUser, username));

            return result.RankingPoints;
        }

        public async Task<List<User>> GetUsersThatCanBeJuryAsync()
        {
            int minPointsForJury = 150;

            var result = await this.context.Users
                .Include(u => u.ContestItems)
                .Where(u => u.RankingPoints > minPointsForJury)
                .ToListAsync();

            if (result.Count == 0)
            {
                throw new ArgumentException(ErrorMessagesTypes.UserListIsEmpty);
            }

            return result;
        }

        public async Task<List<User>> GetUsersThatCanBeParticipantsAsync()
        {
            int minPointsForJury = 151;

            var result = await this.context.Users
                .Include(u => u.ContestItems)
                .Where(u => u.RankingPoints < minPointsForJury)
                .ToListAsync();

            if (result.Count == 0)
            {
                throw new ArgumentException(ErrorMessagesTypes.UserListIsEmpty);
            }

            return result;
        }

        public async Task<string> GetRankingInformationAsync(string username)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Username == username);

            if (user == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessagesTypes.NotExistingUser, username));
            }

            var points = await this.rankService.PointsToNextRankAsync(user.RankingPoints);

            string userRank = await this.rankService.GetJunkieRankByPointsAsync(user.RankingPoints);

            string result = $"Points: {user.RankingPoints}, Rank: {userRank}, Points to next rank: {points}";

            return result;
        }

        public async Task<bool> IsEnrolledAsync(string username, int contestId)
        {
            var result = await this.context.Users
                .Include(u => u.ContestItems)
                .AnyAsync(u => u.Username.ToLower().Equals(username.ToLower()) && u.ContestItems.Any(ci => ci.ContestId == contestId));

            return result;
        }

        public bool IsEnrolled(User user, int contestId)
        {
            var result = user.ContestItems.Any(ci => ci.ContestId == contestId);
            return result;
        }

        public async Task<bool> IsInvitedAsync(string username, int contestId)
        {
            var user = await this.context.Users
                .Include(u => u.InvitedUsers)
                .FirstOrDefaultAsync(u => u.Username.ToLower().Equals(username.ToLower()));

            var contest = await this.contestService.GetAsync(contestId);

            if (!user.InvitedUsers.Any(iu => iu.ContestId == contestId) || contest.ContestType.ToLower().Equals("open".ToLower()))
            {
                return false;
            }

            return true;
        }

        

        public async Task<List<UserDTO>> OrderUsersByRatingAsync()
        {
            var result = await this.context.Users
                .OrderByDescending(u => u.RankingPoints)
                .ToListAsync();

            return UserListToUserDTOList(result);
        }

        public async Task<UserDTO> UpdatePointsAsync(User user, int updatePoints)
        {
            if (user == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessagesTypes.NotExistingUser, user.UserId));
            }

            if ((user.RankingPoints + updatePoints) < 0)
            {
                user.RankingPoints = 0;
            }
            else
            {
                user.RankingPoints += updatePoints;
            }

            var result = new UserDTO(user);

            this.context.Users.Update(user);
            await this.context.SaveChangesAsync();

            return result;
        }


        private List<UserDTO> UserListToUserDTOList(List<User> users)
        {
            var usersDTO = new List<UserDTO>();
            foreach (var user in users)
            {
                usersDTO.Add(new UserDTO(user));
            }

            return usersDTO;
        }

        
    }
}
