﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CategoryServiceTests
{
    [TestClass]
    public class TryGetAllCategories_Should
    {
        [TestMethod]
        public async Task ReturnAllCategoriesCorrectAsync()
        {
            var context = Utils.GetContext(nameof(ReturnAllCategoriesCorrectAsync));
            var sut = new CategoryService(context);
            var allCategories = context.Categories.ToList();

            //Act
            var result = await sut.GetAllCategoriesAsync();

            //Assert
            Assert.AreEqual(result.Count, allCategories.Count);
        }
    }
}
