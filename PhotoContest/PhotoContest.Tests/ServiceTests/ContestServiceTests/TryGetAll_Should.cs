﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Database.Models;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class TryGetAll_Should
    {
        private Mock<IContestTypeService> mockContestTypeService = new Mock<IContestTypeService>();
        private Mock<IFileService> mockFileService = new Mock<IFileService>();
        private Mock<IOrganizerService> mockOrganaizerService = new Mock<IOrganizerService>();
        private Mock<IContestJuryService> mockContestJuryService = new Mock<IContestJuryService>();
        private Mock<ICategoryService> mockCategoryService = new Mock<ICategoryService>();
        private Mock<IInvitedUsers> mockInvitedUserservice = new Mock<IInvitedUsers>();

        [TestMethod]
        public async Task ReturnAllSuccessContestsAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(ReturnAllSuccessContestsAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var expected = context.Contests.ToList();

            //Act
            var result = await sut.GetAllAsync();

            //Assert
            Assert.AreEqual(expected.Count, result.Count);
        }

        [TestMethod]
        public async Task GetContestsSuccessAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestsSuccessAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            
            var expected = context.Contests.FirstOrDefault(c => c.ContestId == 1);

            //Act
            var result = await sut.GetAsync(1);

            //Assert
            Assert.AreEqual(expected.ContestId, result.ContestId);
            Assert.AreEqual(expected.Title, result.Title);
            Assert.AreEqual(expected.Category.Name, result.Category);
            Assert.AreEqual(expected.CreateOnDate, result.CreateOnDate);
            Assert.AreEqual(expected.TimeLimitPhaseOne, result.TimeLimitPhaseOne);
            Assert.AreEqual(expected.TimeLimitPhaseTwo, result.TimeLimitPhaseTwo);
        }

        
        [TestMethod]
        public async Task GetContestsFailAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestsFailAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var id = 1111;


            //Act & Assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.GetAsync(id));
            
        }

        [TestMethod]
        public void GetContestInPhaseOne()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestInPhaseOne));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var contest = new Contest();
            contest.CreateOnDate = DateTime.UtcNow;
            contest.TimeLimitPhaseOne = 10;
            contest.TimeLimitPhaseTwo = 24;

            //Act
            var result = sut.GetContestPhase(contest).ToLower();


            // Assert
            Assert.AreEqual("phase i", result);
        }

        [TestMethod]
        public void GetContestInPhaseTwo()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestInPhaseTwo));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var contest = new Contest();
            contest.CreateOnDate = DateTime.UtcNow.AddDays(-1);
            contest.TimeLimitPhaseOne = 1;
            contest.TimeLimitPhaseTwo = 24;

            //Act
            var result = sut.GetContestPhase(contest).ToLower();


            // Assert
            Assert.AreEqual("phase ii", result);
        }

        [TestMethod]
        public void GetContestInPhaseFinished()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestInPhaseFinished));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var contest = new Contest();
            contest.CreateOnDate = DateTime.UtcNow.AddDays(-10);
            contest.TimeLimitPhaseOne = 1;
            contest.TimeLimitPhaseTwo = 1;

            //Act
            var result = sut.GetContestPhase(contest).ToLower();


            // Assert
            Assert.AreEqual("finished", result);
        }

        [TestMethod]
        public void GetContestInPhaseNotStarted()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestInPhaseNotStarted));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var contest = new Contest();
            contest.CreateOnDate = DateTime.UtcNow.AddDays(20);
            contest.TimeLimitPhaseOne = 1;
            contest.TimeLimitPhaseTwo = 1;

            //Act
            var result = sut.GetContestPhase(contest).ToLower();


            // Assert
            Assert.AreEqual("not started", result);
        }

        [TestMethod]
        public async Task GetContestsThatIsInJurySuccessAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestsThatIsInJurySuccessAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            var userId = 4;
            var contestId = 1;
            var exptected = context.ContestJuries.Where(cj => cj.UserId == userId && cj.ContestId == contestId).Count();
            
            //Act
            var result = await sut.GetContestsThatIsInJury(userId);

            // Assert
            Assert.AreEqual(exptected, result[0].ContestId);
            
        }

        [TestMethod]
        public async Task GetContestsThatIsInJuryNoneAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetContestsThatIsInJuryNoneAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var fakeUserId = 1111;

            //Act
            var result = await sut.GetContestsThatIsInJury(fakeUserId);

            // Assert
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task GetJunkieContests()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetJunkieContests));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var userId = 3;
            var contestId = 6;

            var expected = context.ContestItems.Where(ci => ci.UserId == userId && ci.ContestId == contestId);

            //Act
            var result = await sut.GetJunkieContestsAsync(userId);

            // Assert
            Assert.AreEqual(expected.Count(), result.Count);
        }

        [TestMethod]
        public async Task GetJunkieContestsFailAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetJunkieContestsFailAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var fakeUserId = 1000;

            //Act
            var result = await sut.GetJunkieContestsAsync(fakeUserId);

            // Assert
            Assert.AreEqual(0, result.Count);
        }



        [TestMethod]
        public async Task GetOpenContestsAsync()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetOpenContestsAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            var contestType = context.ContestTypes.FirstOrDefault(ct => ct.Name.ToLower().Equals("open"));
            var expected = context.Contests.Where(c => c.ContestTypeId == contestType.ContestTypeId).ToList();

            //Act
            var result = await sut.GetAllOpenContestsAsync();

            // Assert
            Assert.AreEqual(expected.Count, result.Count);
        }

        [TestMethod]
        public void TimeOfPhaseOneUtilEnd()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetOpenContestsAsync));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var startDate = DateTime.UtcNow;
            TimeSpan expected = startDate.AddDays(days).Subtract(DateTime.UtcNow);

            //Act
            var result = sut.CountDownPhaseOneTimeSpan(startDate, days);

            // Assert
            Assert.AreEqual ((int)expected.TotalMinutes, ((int)result.TotalMinutes));
        }

        [TestMethod]
        public void TimeOfPhaseTwoUtilEnd()
        {
            // Arange
            var context = Utils.GetContext(nameof(TimeOfPhaseTwoUtilEnd));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;
            TimeSpan expected = startDate.AddDays(days).AddHours(hours).Subtract(DateTime.UtcNow);

            //Act
            var result = sut.CountDownPhaseTwoTimeSpan(startDate, days, hours);

            // Assert
            Assert.AreEqual((int)expected.TotalMinutes, (int)result.TotalMinutes);
        }

        [TestMethod]
        public async Task GetTimeToNextPhaseNotStartedSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseNotStartedSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;
            
            var contest = new Contest();
            contest.CreateOnDate = startDate.AddMinutes(1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.AreEqual("not started", result.ToLower());

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseNotStartedFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseNotStartedFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.AreNotEqual("not started", result.ToLower());

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseFinishedSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseFinishedSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddHours(-hours).AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.AreEqual("finished", result.ToLower());

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseFinishedFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseFinishedFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddHours(-hours).AddMinutes(1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.AreNotEqual("finished", result.ToLower());

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseOneStartSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseOneStartSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase i "));

        }


        [TestMethod]
        public async Task GetTimeToNextPhaseOneStartFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseOneStartFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddMinutes(1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase i "));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseOneEndSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseOneEndSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase i "));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseOneEndFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseOneEndFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase i "));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseTwoStartSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseTwoStartSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsTrue(result.ToLower().Contains("phase ii"));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseTwoStartFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseTwoStartFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddMinutes(1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase ii"));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseTwoEndSuccess()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseTwoEndSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddHours(-hours).AddMinutes(1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsTrue(result.ToLower().Contains("phase ii"));

        }

        [TestMethod]
        public async Task GetTimeToNextPhaseTwoEndFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(GetTimeToNextPhaseTwoEndFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var days = 1;
            var hours = 24;
            var startDate = DateTime.UtcNow;

            var contest = new Contest();
            contest.CreateOnDate = startDate.AddDays(-days).AddHours(-hours).AddMinutes(-1);
            contest.TimeLimitPhaseOne = days;
            contest.TimeLimitPhaseTwo = hours;
            context.Contests.Add(contest);
            context.SaveChanges();
            var addedContest = context.Contests.LastOrDefault();

            //Act
            var result = await sut.GetTimeToNextPhaseAsync(addedContest.ContestId);

            // Assert
            Assert.IsFalse(result.ToLower().Contains("phase ii"));

        }

        [TestMethod]
        public async Task IsContestJurySuccess()
        {
            // Arange
            var contestId = 1; 
            var username = "ivanov";
            var contestJuries = new List<ContestJury>();
            var contestJury = new ContestJury
            {
                ContestJuryId = 1,
                UserId = 1,
                ContestId = contestId,
            };
            
            contestJuries.Add(contestJury);
            
            this.mockContestJuryService.Setup(cj => cj.GetAllForContestAsync(contestId)).ReturnsAsync(contestJuries);
            this.mockContestJuryService.Setup(cj => cj.GetAsync(username)).ReturnsAsync(contestJury);

            var context = Utils.GetContext(nameof(IsContestJurySuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
           
                
            //Act
            var result = await sut.IsContestJury(contestId, username);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task IsContestJuryFail()
        {
            // Arange
            var contestId = 1;
            var username = "ivanov";
            var contestJury = new ContestJury
            {
                ContestJuryId = 1,
                UserId = 1,
                ContestId = contestId,
            };
            var contestJuries = new List<ContestJury>
            {
                new ContestJury
                {
                    ContestJuryId = 2,
                    UserId = 2,
                    ContestId = contestId,
                },
                new ContestJury
                {
                    ContestJuryId = 3,
                    UserId = 3,
                    ContestId = contestId,
                }
            };

            this.mockContestJuryService.Setup(cj => cj.GetAllForContestAsync(contestId)).ReturnsAsync(contestJuries);
            this.mockContestJuryService.Setup(cj => cj.GetAsync(username)).ReturnsAsync(contestJury);

            var context = Utils.GetContext(nameof(IsContestJuryFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            //Act
            var result = await sut.IsContestJury(contestId, username);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public async Task IsContestOrganizerSuccess()
        {
            // Arange
            var username = "ivanov";
            var organaizer = new Organizer
            {
                OrganaizerId = 1,
                Username = username
            };

            this.mockOrganaizerService.Setup(o => o.GetByUsernameAsync(username)).ReturnsAsync(organaizer);
            
            var context = Utils.GetContext(nameof(IsContestOrganizerSuccess));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            //Act
            var result = await sut.IsContestOrganizerJury(username);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task IsContestOrganizerFail()
        {
            // Arange
            var username = "ivanov";
            Organizer organaizer = null;

            this.mockOrganaizerService.Setup(o => o.GetByUsernameAsync(username)).ReturnsAsync(organaizer);

            var context = Utils.GetContext(nameof(IsContestOrganizerFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            //Act
            var result = await sut.IsContestOrganizerJury(username);

            // Assert
            Assert.IsFalse(result);
        }

         [TestMethod]
        public async Task CreateContestAsyncTitleFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestAsyncTitleFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;  
            ContestDTO contest = new ContestDTO
            {
                Title = "T"
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));

            // Assert
            Assert.AreEqual(ErrorMessagesTypes.TitleFailValidation, ex.Message);
        
        }

        [TestMethod]
        public async Task CreateContestAsyncPhaseOneTimeLimitMaxFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestAsyncPhaseOneTimeLimitMaxFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 32
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));

            // Assert
            Assert.AreEqual(ErrorMessagesTypes.PhaseOneValidation, ex.Message);
        }

        [TestMethod]
        public async Task CreateContestAsyncPhaseOneTimeLimitMinFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestAsyncPhaseOneTimeLimitMinFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 0
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));

            // Assert
            Assert.AreEqual(ErrorMessagesTypes.PhaseOneValidation, ex.Message);
        }


        [TestMethod]
        public async Task CreateContestAsyncPhaseTwoimeLimitMaxFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestAsyncPhaseTwoimeLimitMaxFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 1,
                TimeLimitPhaseTwo = 25
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));

            // Assert
            Assert.AreEqual(ErrorMessagesTypes.PhaseTwoValidation, ex.Message);
        }

        [TestMethod]
        public async Task CreateContestAsyncPhaseTwoTimeLimitMinFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestAsyncPhaseTwoTimeLimitMinFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 1,
                TimeLimitPhaseTwo = 0
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));

            // Assert
            Assert.AreEqual(ErrorMessagesTypes.PhaseTwoValidation, ex.Message);
        }

        [TestMethod]
        public async Task CreateContestTypeFail()
        {
            // Arange
            var context = Utils.GetContext(nameof(CreateContestTypeFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);
            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 1,
                TimeLimitPhaseTwo = 1,
                ContestType = "Not existing"
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act & Assert
            await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreateContestAsync(contest, creatorId, selectedJuries));
        }

        [TestMethod]
        public async Task CreateContestCategoryFail()
        {
            // Arange
            var categoryId = 222;
            var typeId = 111;
            var category = new Category { CategoryId = categoryId, Name = "Test Category" };
            var type = new ContestType { ContestTypeId = typeId, Name = "Test Type" };
 
            this.mockCategoryService.Setup(o => o.GetCategoryByIdAsync(categoryId)).ReturnsAsync(category);

            var context = Utils.GetContext(nameof(CreateContestCategoryFail));
            var sut = new ContestService(context, this.mockContestTypeService.Object, this.mockFileService.Object,
                                         this.mockOrganaizerService.Object, this.mockContestJuryService.Object,
                                         this.mockCategoryService.Object, this.mockInvitedUserservice.Object);

            context.ContestTypes.Add(type);
            await context.SaveChangesAsync();

            var creatorId = 5;
            ContestDTO contest = new ContestDTO
            {
                Title = "Valid name",
                TimeLimitPhaseOne = 1,
                TimeLimitPhaseTwo = 1,
                ContestType = typeId.ToString(),
                CreateOnDate = DateTime.UtcNow,
                Category = categoryId.ToString()
            };
            var selectedJuries = new List<int> { 1, 2, 3 };

            // Act
            var result = await sut.CreateContestAsync(contest, creatorId, selectedJuries);

            // Assert
            Assert.AreEqual(contest.Title, result.Title);
            Assert.AreEqual(contest.TimeLimitPhaseOne, result.TimeLimitPhaseOne);
            Assert.AreEqual(contest.TimeLimitPhaseTwo, result.TimeLimitPhaseTwo);
            Assert.AreEqual(type.Name, result.ContestType);
            Assert.AreEqual(contest.CreateOnDate, result.CreateOnDate);
            Assert.AreEqual(category.Name, result.Category);
        }
    }
}
