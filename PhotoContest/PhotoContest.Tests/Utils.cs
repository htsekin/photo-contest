﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.IO;

namespace PhotoContest.Tests
{
    public class Utils
    {
        public static PhotoDbContext GetContext(string databaseName)
        {
            var options = new DbContextOptionsBuilder<PhotoDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;

            var context = new PhotoDbContext(options);

            Utils.SeedData(context);

            return context;
        }

        private static void SeedData(PhotoDbContext context)
        {
            // Password hashers
            var passHasherUser = new PasswordHasher<User>();
            var passHasherOrganaizer = new PasswordHasher<Organizer>();

            string projectDir = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\.."));
            var seedFilesPath = @"\PhotoContest.Database\SeedDataFiles\";
            var fullPathToSeedFiles = projectDir + seedFilesPath;

            var ranks = JsonConvert.DeserializeObject<List<Rank>>(File.ReadAllText(fullPathToSeedFiles + "rank.json"));
            var categories = JsonConvert.DeserializeObject<List<Category>>(File.ReadAllText(fullPathToSeedFiles + "category.json"));
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(fullPathToSeedFiles + "user.json"));
            var contestTypes = JsonConvert.DeserializeObject<List<ContestType>>(File.ReadAllText(fullPathToSeedFiles + "contesttype.json"));
            var organaizers = JsonConvert.DeserializeObject<List<Organizer>>(File.ReadAllText(fullPathToSeedFiles + "organaizer.json"));
            var contests = JsonConvert.DeserializeObject<List<Contest>>(File.ReadAllText(fullPathToSeedFiles + "contest.json"));
            var contestItems = JsonConvert.DeserializeObject<List<ContestItem>>(File.ReadAllText(fullPathToSeedFiles + "contestitem.json"));
            var reviews = JsonConvert.DeserializeObject<List<Review>>(File.ReadAllText(fullPathToSeedFiles + "review.json"));
            var contestJuries = JsonConvert.DeserializeObject<List<ContestJury>>(File.ReadAllText(fullPathToSeedFiles + "selectedjury.json"));
            var invitedUsers = JsonConvert.DeserializeObject<List<InvitedUser>>(File.ReadAllText(fullPathToSeedFiles + "inviteduser.json"));


            foreach (var item in users)
            {
                item.Password = passHasherUser.HashPassword(item, item.Password);
            }

            foreach (var item in organaizers)
            {
                item.Password = passHasherOrganaizer.HashPassword(item, item.Password);
            }

            context.Ranks.AddRangeAsync(ranks);
            context.Categories.AddRangeAsync(categories);
            context.Users.AddRangeAsync(users);
            context.ContestTypes.AddRangeAsync(contestTypes);
            context.Organaizers.AddRangeAsync(organaizers);
            context.Contests.AddRangeAsync(contests);
            context.ContestItems.AddRangeAsync(contestItems);
            context.Reviews.AddRangeAsync(reviews);
            context.ContestJuries.AddRangeAsync(contestJuries);
            context.InvitedUsers.AddRangeAsync(invitedUsers);

            context.SaveChangesAsync();
        }
        public static DbContextOptions<PhotoDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PhotoDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}

