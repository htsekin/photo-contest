﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Helpers;
using PhotoContest.Web.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContestController : ControllerBase
    {
        private readonly IContestService contestService;
        private readonly IContestItemService contestItemService;
        private readonly IUserService userService;
        private readonly ICategoryService categoryService;
        private readonly IAuthHelper authHelper;

        public ContestController(IContestService contestService, IContestItemService contestItemService, IUserService userService,
            ICategoryService categoryService, IAuthHelper authHelper)
        {
            this.contestService = contestService;
            this.contestItemService = contestItemService;
            this.userService = userService;
            this.categoryService = categoryService;
            this.authHelper = authHelper;
        }

        [HttpGet]
        [Route("timetilphasetwo")]
        public async Task<IActionResult> GetTimeUntilPhaseTwoAsync([FromHeader] string username, int contestId)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                var user = await this.authHelper.GetUserByUsername(username);

                if (organizer == null && user == null)
                    return BadRequest("Invalid Authentication!");

                var contest = await this.contestService.GetAsync(contestId);

                var phase = this.contestService.GetContestPhase(contest);

                if (phase.ToLower() != "Phase One".ToLower())
                {
                    return BadRequest("Contest is not in Phase One!");
                }

                var result = await this.contestService.GetTimeToNextPhaseAsync(contestId);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
