﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IContestItemService contestItemService;
        private readonly IUserService userService;
        private readonly IOrganizerService organizerService;

        public HomeController(IContestItemService contestItemService, IUserService userService, IOrganizerService organizerService )
        {
            this.contestItemService = contestItemService;
            this.userService = userService;
            this.organizerService = organizerService;
        }

        [HttpGet]
        [Route("toptenphotos")]
        public async Task<IActionResult> GetTopTenPhotosAsync()
        {
            return Ok(await this.contestItemService.GetTopTen());
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterUserAsync([FromBody] RegisterDTO newUser)
        {
            try
            {
                var user = await this.userService.GetAsync(newUser.Username);

                var organizer = await this.organizerService.GetByUsernameAsync(newUser.Username);

                if (user != null || organizer != null) 
                {
                    return BadRequest("User already exists!");
                }

                var result = await this.userService.CreateUserAsync(newUser);

                return Created("New user was created!", result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
