﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JunkieController : ControllerBase
    {
        private readonly IAuthHelper authHelper;
        private readonly IContestItemService contestItemService;
        private readonly IContestService contestService;
        private readonly IUserService userService;
        private readonly IReviewService reviewService;
        public JunkieController(IAuthHelper authHelper, IContestItemService contestItemService, IContestService contestService,
            IUserService userService, IReviewService reviewService)
        {
            this.authHelper = authHelper;
            this.contestItemService = contestItemService;
            this.contestService = contestService;
            this.userService = userService;
            this.reviewService = reviewService;
        }

        [HttpGet]
        [Route("getitems")]
        public async Task<IActionResult> GetJunkieContestItemsAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);
                
                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestItemService.GetItemForUserAsync(junkie.UserId);

                if (result.Count == 0)
                {
                    return BadRequest($"User {junkie.Username} does not have any photos!");
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            
        }

        [HttpGet]
        [Route("contests")]
        public async Task<IActionResult> GetAllOpenContestsAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetAllOpenContestsAsync();

                if (result.Count == 0)
                {
                    return BadRequest("There are not any Open contests!");
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("finishedcontests")]
        public async Task<IActionResult> GetAllContestItemsInFinhedContestAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");
                
                var contests = await this.contestService.GetJunkieContestsByPhaseAsync(junkie.UserId, "Finished");

                if (contests.Count == 0)
                {
                    return BadRequest($"User {junkie.Username} does not participate in any Finished contests!");
                }
                else
                {
                    return Ok(contests);
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("mycontests")]
        public async Task<IActionResult> JunkieContestsAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetJunkieContestsAsync(junkie.UserId);

                if (result.Count == 0)
                {
                    return BadRequest($"Junkie with username {junkie.Username} does not have any contests!");
                }

                return Ok(result);

            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("myfinishcontests")]
        public async Task<IActionResult> JunkieFinishedContestsAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetJunkieContestsByPhaseAsync(junkie.UserId, "Finished");

                if (result.Count == 0)
                {
                    return BadRequest($"Junkie with username {junkie.Username} does not have any finished contests!");
                }

                return Ok(result);

            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("pointsinfo")]
        public async Task<IActionResult> GetPointsInfoAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.userService.GetRankingInformationAsync(junkie.Username);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("finishreviews")]
        public async Task<IActionResult> GetReviewsForItemAsync([FromHeader] string username, [FromQuery] int itemId)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                //var contests = await this.contestService.GetJunkieContestsByPhaseAsync(junkie.UserId, "Finished");

                // TODO: check if contest is in phase finished

                var result = await this.reviewService.GetAllReviewsForContestItemAsync(itemId);

                if (result.Count == 0)
                {
                    return BadRequest($"User {junkie.Username} does not have any reviews for item with id: {itemId}");
                }

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("enroll")]
        public async Task<IActionResult> EnrollToContestAsync([FromHeader] string username, [FromBody] ContestItemDTO contestItem, [FromQuery] int contestId)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var contest = await this.contestService.GetAsync(contestId);

                var phase = this.contestService.GetContestPhase(contest);

                var participants = await this.userService.GetAllContestParticipantsAsync(contest.ContestId);

                var isParticipant = !participants.Any(u => u.Username == junkie.Username);

                if (contest.ContestType.ToLower().Equals("open".ToLower()) && phase.ToLower().Equals("phase one".ToLower()) && isParticipant)
                {
                    if (!await this.userService.IsEnrolledAsync(junkie.Username, contestId))
                    {
                        if (await this.userService.IsInvitedAsync(junkie.Username, contestId))
                        {
                            var result = await this.contestItemService.EnrollToContestAsync(username, contestId, contestItem);

                            return Created($"{junkie.Username} is invited to this contest!", result);
                        }
                        else
                        {
                            var result = await this.contestItemService.EnrollToContestAsync(username, contestId, contestItem);

                            return Ok(result);
                        }
                    }
                    else
                    {
                        return BadRequest($"User {username} can upload only one Photo per contest!");
                    }
                }
                else
                {
                    return BadRequest("Contest is not in Phase One!");
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("timetilfinish")] 
        public async Task<IActionResult> GetTimeUntilFinishPhaseAsync([FromHeader] string username, [FromQuery] int contestId)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var contest = await this.contestService.GetAsync(contestId);

                // TODO format string
                var result = this.contestService.CountDownPhaseTwoTimeSpan(contest.CreateOnDate, contest.TimeLimitPhaseOne, contest.TimeLimitPhaseTwo);


                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("finishphase")]
        public async Task<IActionResult> GetScoreAndCommentsAsync([FromHeader] string username)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestItemService.GetItemsForUserInFinishedAsync(junkie.UserId);

                if (result.Count == 0)
                {
                    return BadRequest($"User {junkie.Username} does not have photos in any Finished contest!");
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("finishviewall")]
        public async Task<IActionResult> GetAllItemsForContestAsync([FromHeader] string username, [FromQuery] int contestId)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestItemService.GetAllContestItemsForContestAsync(contestId);

                // TODO: check if contest is in phase finished

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateContestItemAsync([FromHeader] string username, [FromQuery] int userId, int contestId, [FromBody] ContestItemDTO newContestItem)
        {
            try
            {
                var junkie = await this.authHelper.GetUserByUsername(username);

                if (junkie == null)
                    return BadRequest("Invalid Authentication!");

                var contest = await this.contestService.GetAsync(contestId);

                var result = await this.contestItemService.CreateContestItemAsync(junkie.Username, contest.ContestId, newContestItem);

                return Created("", result);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
