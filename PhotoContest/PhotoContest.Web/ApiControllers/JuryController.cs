﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JuryController : ControllerBase
    {
        private readonly IAuthHelper authHelper;
        private readonly IContestJuryService contestJuryService;
        private readonly IContestItemService contestItemService;
        private readonly IContestService contestService;
        private readonly IUserService userService;
        private readonly IOrganizerService organizerService;
        private readonly IReviewService reviewService;

        public JuryController(IAuthHelper authHelper, IContestJuryService contestJuryService, IContestItemService contestItemService,
            IContestService contestService, IUserService  userService, IOrganizerService organizerService,
            IReviewService reviewService)
        {
            this.authHelper = authHelper;
            this.contestJuryService = contestJuryService;
            this.contestItemService = contestItemService;
            this.contestService = contestService;
            this.userService = userService;
            this.organizerService = organizerService;
            this.reviewService = reviewService;
        }

        [HttpGet]
        [Route("photos")]
        public async Task<IActionResult> GetAllSubmitedPhotosInPhaseOne([FromHeader] string username, int contestId) 
        {
            try
            {
                var organizerAuth = await this.authHelper.GetOrganizerByUsername(username);

                var userAuth = await this.authHelper.GetUserByUsername(username);

                if (organizerAuth == null && userAuth == null)
                    return BadRequest("Invalid Authentication!");


                var organizer = organizerAuth;

                var user = userAuth;

                if (user != null || organizer == null)
                {
                    var isJuryUser = await this.contestService.IsContestJury(contestId, user.Username);

                    if (isJuryUser == false)
                    {
                        return BadRequest("User is not jury!");
                    }
                }

                var contest = await this.contestService.GetAsync(contestId);

                var phase = this.contestService.GetContestPhase(contest);


                if (phase.ToLower().Equals("Phase I".ToLower()))
                {
                    var photos = await this.contestItemService.GetAllContestItemsForContestAsync(contest.ContestId);

                    return Ok(photos);
                }
                else
                {
                    return BadRequest("Contest is not in Phase I");
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> UpdateReviewAsync([FromHeader] string username, [FromBody] ReviewDTO newReview, [FromQuery] int contestId, int itemId)
        {
            var isJury = await this.contestService.IsContestJury(contestId, username);
            var isOrgJury = await this.contestService.IsContestOrganizerJury(username);

            var organizerAuth = await this.authHelper.GetOrganizerByUsername(username);

            var userAuth = await this.authHelper.GetUserByUsername(username);

            if (organizerAuth == null && userAuth == null)
                return BadRequest("Invalid Authentication!");

            var jury = await this.contestJuryService.GetAsync(username);

            string j = null;
            if (jury != null)
                j = jury.User.Username;

            var organizer = await this.organizerService.GetByUsernameAsync(username);

            if (isJury || isOrgJury)
            {
                var contest = await this.contestService.GetAsync(contestId);

                var phase = this.contestService.GetContestPhase(contest);

                if (phase.ToLower().Equals("Phase II".ToLower()))
                {
                    if (organizer != null)
                    {
                        var review = await this.reviewService.UpdateReviewAsync(newReview, j, organizer.Username, itemId);

                        return Ok(review);
                    }
                    else if (jury != null)
                    {
                        var review = await this.reviewService.UpdateReviewAsync(newReview, j, organizer.Username, itemId);

                        return Ok(review);
                    }
                }
                else
                {
                    return BadRequest("Contest is not in Phase II");
                }
            }

            return BadRequest($"User with username {username} cannot update review!");
        }
    }
}
