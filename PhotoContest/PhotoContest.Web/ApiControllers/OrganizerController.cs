﻿using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizerController : ControllerBase
    {
        private readonly IAuthHelper authHelper;
        private readonly IContestService contestService;
        private readonly IUserService userService;
        private readonly IContestItemService contestItemService;
        private readonly IOrganizerService organizerService;

        // TODO POST - create contest

        public OrganizerController(IAuthHelper authHelper, IContestService contestService, IUserService userService,
            IContestItemService contestItemService, IOrganizerService organizerService)
        {
            this.authHelper = authHelper;
            this.contestService = contestService;
            this.userService = userService;
            this.contestItemService = contestItemService;
            this.organizerService = organizerService;
        }

        [HttpGet]
        [Route("contestitems")]
        public async Task<IActionResult> GetAllContestItemsAsync([FromHeader] string username, [FromQuery] int contestId)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestItemService.GetAllContestItemsForContestAsync(contestId);

                if (result.Count == 0)
                {
                    return BadRequest("No items in this contest!");
                }

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("phaseone")]
        public async Task<IActionResult> GetAllContestsInPhaseOneAsync([FromHeader] string username)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetContestsByPhaseAsync("Phase I".ToLower());
                
                if (result.Count == 0)
                {
                    return BadRequest("No contests in Phase I");
                }

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        

        [HttpGet]
        [Route("phasetwo")]
        public async Task<IActionResult> GetAllContestsInPhaseTwoAsync([FromHeader] string username)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetContestsByPhaseAsync("Phase II");

                if (result.Count == 0)
                {
                    return BadRequest("No contests in Phase II");
                }

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("finished")]
        public async Task<IActionResult> GetAllContestsInPhaseFinishedAsync([FromHeader] string username)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.contestService.GetContestsByPhaseAsync("Finished");

                if (result.Count == 0)
                {
                    return BadRequest("No contests in phase Finished");
                }

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet]
        [Route("junkies")]
        public async Task<IActionResult> GetAllJunkiesAsync([FromHeader] string username)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var result = await this.userService.GetAllAsync();

                result = result.OrderByDescending(u => u.RankingPoints).ToList();

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("createcontest")]
        public async Task<IActionResult> CreateContestAsync([FromHeader] string username, [FromBody] ContestDTO newContest)
        {
            try
            {
                var organizer = await this.authHelper.GetOrganizerByUsername(username);

                var creator = await this.organizerService.GetByUsernameAsync(organizer.Username);

                if (organizer == null)
                    return BadRequest("Invalid Authentication!");

                var contest = await this.contestService.CreateContestAsync(newContest, creator.OrganaizerId, newContest.JuriesIds);

                return Ok(contest);

            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
