﻿using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models;
using System.Threading.Tasks;

namespace PhotoContest.Web.ViewComponents
{
    public class PhaseOneViewComponent : ViewComponent
    {
        private readonly IContestItemService contestItemService;
        private readonly IContestService contestService;

        public PhaseOneViewComponent(IContestItemService contestItemService, IContestService contestService)
        {
            this.contestItemService = contestItemService;
            this.contestService = contestService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var model = new PhaseOneViewModel();
            model.ContestId = id;
            return View(model);
        }
    }
}
