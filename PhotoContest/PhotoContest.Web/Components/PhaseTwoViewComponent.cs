﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models;
using System.Threading.Tasks;

namespace PhotoContest.Web.ViewComponents
{
    public class PhaseTwoViewComponent : ViewComponent
    {
        private readonly IReviewService reviewService;
        private readonly IContestItemService contestItemService;

        public PhaseTwoViewComponent(IReviewService reviewService, IContestItemService contestItemService)
        {
            this.reviewService = reviewService;
            this.contestItemService = contestItemService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int contestId, int contestItemId)
        {
            var model = new PhaseTwoViewModel();
            //model.ContestItems = MapperConfig.Mapper.ContestItemsDtoToContestItemsVM(await this.contestItemService.GetAllContestItemsForContestAsync(id));
            model.ContestId = contestId;
            model.ContestItemId = contestItemId;
            
            return View(model);
        }
    }
}
