﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Web.Components
{
    public class RankViewComponent : ViewComponent
    {
        private readonly IRankService rankService;
        private readonly IUserService userService;

        public RankViewComponent(IRankService rankService, IUserService userService)
        {
            this.rankService = rankService;
            this.userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            StringBuilder result = new StringBuilder();
            result.Append("");

            var currentUser = this.HttpContext.Session.GetString("junkieUsername");
            if(currentUser != null)
            {
                var user = await this.userService.GetAsync(currentUser);
                var rank = await this.rankService.GetJunkieRankByPointsAsync(user.RankingPoints);
                result.Append(rank);
            }

            var currentOrganaizer = this.HttpContext.Session.GetString("organizerUsername");
            if (currentOrganaizer != null)
            {
                result.Append("Organaizer");
            }

            
            return View(result);
            
        }
    }
}
