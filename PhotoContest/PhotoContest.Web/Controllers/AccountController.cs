﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Database;
using PhotoContest.Database.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IOrganizerService organizerService;
        private readonly IUserService userService;
        private readonly PhotoDbContext photoDbContext;

        public AccountController(PhotoDbContext photoDbContext, IOrganizerService organizerService,
            IUserService userService)
        {
            this.organizerService = organizerService;
            this.userService = userService;
            this.photoDbContext = photoDbContext;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Username) || string.IsNullOrEmpty(model.Password))
                {
                    model.Password = "";
                    ModelState.AddModelError("Username", "Invalid Username.");
                    ModelState.AddModelError("Password", "Invalid Password.");
                    return View(model);
                }

                var organizer = await this.organizerService.GetByUsernameAsync(model.Username);
                var junkie = await this.userService.GetAsync(model.Username);

                if (junkie == null && organizer == null)
                {
                    model.Password = "";
                    ModelState.AddModelError("", "Invalid credentials.");
                    return View(model);
                }


                if (junkie == null)
                {

                    var passHasherUser = new PasswordHasher<Organizer>();

                    if (passHasherUser.VerifyHashedPassword(organizer, organizer.Password, model.Password)
                            != PasswordVerificationResult.Failed)
                    {
                        this.HttpContext.Session.SetString("organizerUsername", organizer.Username);

                        return RedirectToAction("Organizer", "Dashboard");
                    }

                    ModelState.AddModelError("", "Invalid credentials.");
                    return View();
                }
                else
                {
                    var passHasherOrganaizer = new PasswordHasher<User>();

                    if (passHasherOrganaizer.VerifyHashedPassword(junkie, junkie.Password, model.Password)
                            != PasswordVerificationResult.Failed)
                    {
                        this.HttpContext.Session.SetString("junkieUsername", junkie.Username);

                        return RedirectToAction("Junkie", "Dashboard");
                    }

                    ModelState.AddModelError("", "Invalid credentials.");
                    return View();
                }
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Account", "Login"));
            }
        }

        [HttpGet]
        public IActionResult Register()
        {
            try
            {
                var registerViewModel = new RegisterViewModel();

                return View(registerViewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Account", "Register"));
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (this.HttpContext.Session.GetString("junkieUsername") == null)
                {
                    var newUser = new User();
                    var passHasherUser = new PasswordHasher<User>();
                    var tmpUser = await this.userService.GetAsync(model.Username);
                    var tmpOrganizer = await this.organizerService.GetByUsernameAsync(model.Username);

                    if (tmpUser == null && tmpOrganizer == null)
                    {
                        newUser.Username = model.Username;
                        newUser.FirsName = model.FirstName;
                        newUser.LastName = model.LastName;
                        if (model.Password != model.ConfirmPassword)
                        {
                            ModelState.AddModelError("ConfirmPassword", "Password and confirm password does not match.");
                            return View(model);
                        }

                        newUser.Password = passHasherUser.HashPassword(newUser, model.Password);

                        await this.photoDbContext.Users.AddAsync(newUser);
                        await this.photoDbContext.SaveChangesAsync();

                        this.HttpContext.Session.SetString("junkieUsername", newUser.Username);
                    }
                    else
                    {
                        ModelState.AddModelError("Username", "Username Already Exists.");
                        return View(model);
                    }

                    return RedirectToAction("Junkie", "Dashboard");
                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Account", "Register"));
            }
        }

        [HttpGet]
        public IActionResult Logout()
        {
            try
            {

                HttpContext.Session.Remove("organizerUsername");
                HttpContext.Session.Remove("junkieUsername");

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Account", "Logout"));
            }
        }
    }
}

