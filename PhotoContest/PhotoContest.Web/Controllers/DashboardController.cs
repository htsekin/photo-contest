﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers;
using PhotoContest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static PhotoContest.Web.Helpers.CustomAuthorizationAttribute;

namespace PhotoContest.Web.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IContestService contestService;
        private readonly IUserService userService;
        private readonly IOrganizerService organizerService;
        private readonly ICategoryService categoryService;
        private readonly IContestTypeService contestTypeService;
        private readonly IContestItemService contestItemService;
        private readonly IContestJuryService contestJuryService;
        private readonly IReviewService reviewService;
        private readonly IRankService rankService;

        public DashboardController(IContestService contestService,
            IUserService userService, IOrganizerService organizerService,
            ICategoryService categoryService, IContestTypeService contestTypeService,
            IContestItemService contestItemService, IContestJuryService contestJuryService,
            IReviewService reviewService, IRankService rankService)
        {
            this.contestService = contestService;
            this.userService = userService;
            this.organizerService = organizerService;
            this.categoryService = categoryService;
            this.contestTypeService = contestTypeService;
            this.contestItemService = contestItemService;
            this.contestJuryService = contestJuryService;
            this.reviewService = reviewService;
            this.rankService = rankService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Users()
        {
            try
            {
                var users = await this.userService
                .OrderUsersByRatingAsync();

                var usersVM = new List<UserViewModel>();

                foreach (var item in users)
                {
                    var rank = await this.rankService.GetJunkieRankByPointsAsync(item.RankingPoints);
                    usersVM.Add(MapperConfig.Mapper.UserDtoToUserVM(item, rank));
                }
                return View(usersVM);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Users"));
            }
        }

        public async Task<IActionResult> Photos() 
        {
            try
            {
                var photos = await this.contestItemService.GetAllAsync();
                return View(MapperConfig.Mapper.ContestItemsDtoToContestItemsVM(photos));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Photos"));
            }
        }


        public async Task<IActionResult> Contests(string type)
        {
            try
            {
                var model = new ContestsViewModel();

                if (string.IsNullOrWhiteSpace(type) || type.ToLower().Equals("phaseone"))
                {
                    model.SelectedType = "phaseOne";
                    model.Contests = await this.contestService.GetContestsByPhaseAsync("Phase I");
                }
                else if (type.ToLower().Equals("phasetwo"))
                {
                    model.SelectedType = "phaseTwo";
                    model.Contests = await this.contestService.GetContestsByPhaseAsync("Phase II");
                }
                else if (type.ToLower().Equals("finished"))
                {
                    model.SelectedType = "finished";
                    model.Contests = await this.contestService.GetContestsByPhaseAsync("Finished");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Contests"));
            }
        }

        [CustomAuthorizeAttribute()]
        public async Task<IActionResult> Organizer()
        {
            try
            {
                var viewModel = new DashboardOrganaizerViewModel();
                viewModel.ContestPhaseOne = (await this.contestService.GetContestsByPhaseAsync("Phase I")).Take(8).ToList();
                viewModel.ContestPhaseTwo = (await this.contestService.GetContestsByPhaseAsync("Phase II")).Take(8).ToList();
                viewModel.ContestPhaseFinished = (await this.contestService.GetContestsByPhaseAsync("Finished")).Take(8).ToList();
                var currentOrganaizerUsername = this.HttpContext.Session.GetString("organizerUsername");
                var organizer = await this.organizerService.GetByUsernameAsync(currentOrganaizerUsername);
                viewModel.Organaizer = MapperConfig.Mapper.OrganizerDtoToOrganizerVM(MapperConfig.Mapper.OrganizerToOrganizerDTO(organizer));

                var usersVM = new List<UserViewModel>();
                var users = (await this.userService.OrderUsersByRatingAsync()).Take(10).ToList();

                foreach (var item in users)
                {
                    var rank = await this.rankService.GetJunkieRankByPointsAsync(item.RankingPoints);
                    usersVM.Add(MapperConfig.Mapper.UserDtoToUserVM(item, rank));
                }

                viewModel.Users = usersVM;

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Organizer"));
            }
        }

        [CustomAuthorizeAttribute()]
        public async Task<IActionResult> Junkie()
        {
            try
            {
                var viewModel = new DashboardJunkieViewModel();
                var currentUserUsername = this.HttpContext.Session.GetString("junkieUsername");
                var currentUser = await this.userService.GetAsync(currentUserUsername);

                viewModel.User = new UserDTO(currentUser);
                viewModel.OpenContests = await this.contestService.GetAllOpenContestsAsync();
                viewModel.MyRunningContests = await this.contestService.GetJunkieContestsAsync(currentUser.UserId);
                viewModel.MyFinishedContests = await this.contestService.GetJunkieContestsByPhaseAsync(currentUser.UserId, "Finished");
                viewModel.MyRankingInfo = await this.userService.GetRankingInformationAsync(currentUserUsername);
                viewModel.ContestsInvitedIn = currentUser.InvitedUsers.Select(c => new ContestDTO(c.Contest)).ToList();
                viewModel.ContestsJuryIn = await this.contestService.GetContestsThatIsInJury(currentUser.UserId);

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Junkie"));
            }
        }

        [CustomAuthorizeAttribute()]
        [HttpGet]
        public async Task<IActionResult> CreateContest()
        {
            try
            {
                var viewModel = new CreateContestViewModel();

                var listCategories = await this.categoryService.GetAllCategoriesAsync();
                var selectListCategories = new List<SelectListItem>();
                foreach (var item in listCategories)
                {
                    selectListCategories.Add(new SelectListItem(item.CategoryName, item.CategoryId.ToString()));
                }
                viewModel.Categories = selectListCategories;

                var listTypes = await this.contestTypeService.GetAllAsync();
                var selelctedTypes = listTypes.Where(t => t.ContestTypeName.ToLower().Equals("open") || t.ContestTypeName.ToLower().Equals("invitational"));
                var selectListTypes = new List<SelectListItem>();
                foreach (var item in selelctedTypes)
                {
                    var selected = item.ContestTypeName.ToLower().Equals("Open") ? true : false;
                    selectListTypes.Add(new SelectListItem(item.ContestTypeName, item.ContestTypeId.ToString(), selected));
                }
                viewModel.Types = selectListTypes;

                var listJuries = await this.userService.GetUsersThatCanBeJuryAsync();
                var selectListJuries = new List<SelectListItem>();
                foreach (var item in listJuries)
                {
                    selectListJuries.Add(new SelectListItem((item.FirsName + " " + item.LastName), item.UserId.ToString()));
                }
                viewModel.JuriesAsSelList = selectListJuries;

                var listParticipants = await this.userService.GetUsersThatCanBeParticipantsAsync();
                var selectListParticipants = new List<SelectListItem>();
                foreach (var item in listParticipants)
                {
                    selectListParticipants.Add(new SelectListItem((item.FirsName + " " + item.LastName), item.UserId.ToString()));
                }
                viewModel.ParticipantsAsSelList = selectListParticipants;

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "CreateContest"));
            }
        }

        [CustomAuthorizeAttribute()]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateContest(CreateContestViewModel model)
        {
            try
            {
                var newContestDTO = new ContestDTO();

                var currentOrganizerUsername = this.HttpContext.Session.GetString("organizerUsername");
                var organizerObject = await this.organizerService.GetByUsernameAsync(currentOrganizerUsername);
                var selectedJuries = new List<UserDTO>();
                var selectedParticipants = new List<UserDTO>();
                var users = await this.userService.GetAllAsync();
                newContestDTO.File = model.Image;
                newContestDTO.Category = model.Category.ToString();
                newContestDTO.ContestType = model.Type.ToString();
                newContestDTO.Title = model.Title;
                newContestDTO.TimeLimitPhaseOne = model.PhaseOne;
                newContestDTO.TimeLimitPhaseTwo = model.PhaseTwo;
                newContestDTO.CreateOnDate = model.StartDate;

                foreach (var item in model.Juries)
                {
                    selectedJuries.Add(users.FirstOrDefault(u => u.UserId == item));
                }

                newContestDTO.Participants = model.Participants;

                var createdContest = await this.contestService.CreateContestAsync(newContestDTO, organizerObject.OrganaizerId, selectedJuries.Select(u => u.UserId).ToList());

                return RedirectToActionPermanent("Contest", new { id = createdContest.ContestId });
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "CreateContest"));
            }
        }

        [CustomAuthorizeAttribute()]
        [HttpGet]
        public async Task<IActionResult> Contest(int id)
        {
            try
            {
                var contestViewModel = new ContestViewModel();
                var contest = await this.contestService.GetAsync(id);
                var contestItems = await this.contestItemService.GetAllContestItemsForContestAsync(contest.ContestId);

                if (this.contestService.GetContestPhase(contest).ToLower().Equals("finished"))
                {
                    await this.contestItemService.CalcAvarage(contestItems);
                }
                

                contestViewModel.ContestItems = MapperConfig.Mapper.ContestItemsDtoToContestItemsVM(contestItems);
                contestViewModel.ContestId = contest.ContestId;
                contestViewModel.Category = contest.Category;
                contestViewModel.CurrentPhase = this.contestService.GetContestPhase(contest);
                contestViewModel.PhaseOneViewModel = new PhaseOneViewModel();
                contestViewModel.PhaseTwoViewModel = new PhaseTwoViewModel();
                contestViewModel.ContestType = contest.ContestType;
                contestViewModel.Title = contest.Title;
                contestViewModel.Photo = contest.PhotoName != null ? contest.PhotoName : "Contest-Default.jpg";
                contestViewModel.TimeToNextPhase = await this.contestService.GetTimeToNextPhaseAsync(id);


                var win = await this.contestItemService.GetWinners(contestItems, contest);
                contestViewModel.ContestWiners = MapperConfig.Mapper.ContestItemsDTOToWinnersVM(win);



                var currentUserUsername = this.HttpContext.Session.GetString("junkieUsername");

                if (AuthHelper.GetCurrentUserType(this.HttpContext).ToLower().Equals("user"))
                {
                    var currentUser = await this.userService.GetAsync(currentUserUsername);

                    contestViewModel.CurrentUserRaitingPoints = currentUser.RankingPoints;
                    contestViewModel.IsCurrentUserContestJury = currentUser.ContestsJuries.Any(u => u.ContestId == id);
                    contestViewModel.IsEnrolled = this.userService.IsEnrolled(currentUser, contest.ContestId);
                    contestViewModel.IsInvited = this.contestService.IsInvited(id, currentUser);

                    return View(contestViewModel);
                }

                return View(contestViewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "Contest"));
            }
        }

        [CustomAuthorizeAttribute()]
        [HttpGet]
        public async Task<IActionResult> ContestItem(int id)
        {
            try
            {
                var model = await this.contestItemService.GetContestItemAsync(id);

                return View(MapperConfig.Mapper.ContestItemDtoToContestItemVM(model));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "ContestItem"));
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PhaseOne(int id, PhaseOneViewModel model)
        {
            try
            {
                var currrentJunkie = this.HttpContext.Session.GetString("junkieUsername");
                var newContestItemDTO = new ContestItemDTO();

                newContestItemDTO.Title = model.Title;
                newContestItemDTO.Story = model.Story;
                newContestItemDTO.Photo = model.Photo;

                await this.contestItemService.CreateContestItemAsync(currrentJunkie, id, newContestItemDTO);

                return RedirectToActionPermanent("Contest", new { id = id });
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "PhaseOne"));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PhaseTwo(int contestId, int contestItemId, PhaseTwoViewModel model)
        {
            try
            {
                var currrentJunkie = this.HttpContext.Session.GetString("junkieUsername");
                var currrentOrganaizer = this.HttpContext.Session.GetString("organizerUsername");
                var updateReview = new ReviewDTO();

                updateReview.Score = model.Score;
                updateReview.Comment = model.Comment;
                updateReview.IsWrongCategory = model.IsInAppropriateCat;

                await this.reviewService.UpdateReviewAsync(updateReview, currrentJunkie, currrentOrganaizer, contestItemId);

                return RedirectToActionPermanent("Contest", new { id = contestId });
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "PhaseTwo"));
            }
        }
    }
}
