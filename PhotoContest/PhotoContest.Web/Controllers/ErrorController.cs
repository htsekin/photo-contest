﻿using Microsoft.AspNetCore.Mvc;
using PhotoContest.Web.Models;
using System;

namespace PhotoContest.Web.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult NotFound()
        {
            try
            {
                string originalPath = "unknown";
                if (HttpContext.Items.ContainsKey("originalPath"))
                {
                    originalPath = HttpContext.Items["originalPath"] as string;
                }
                return View();
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Dashboard", "NotFound"));
            }
            
        }
    }
}
