﻿using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryService categoryService;
        private readonly IContestItemService contestItemService;
        private readonly IContestService contestService;

        public HomeController(ICategoryService categoryService,
            IContestItemService contestItemService, IContestService contestService)
        {
            this.categoryService = categoryService;
            this.contestItemService = contestItemService;
            this.contestService = contestService;
        }

        public async Task<IActionResult> Index()
        {
            try
            {

                var categories = await this.categoryService.GetAllCategoriesAsync();
                var contestItems = await this.contestItemService.GetTopTen();
                var cnt = (await this.contestService.GetContestsByPhaseAsync("Phase I")).Count;
                var homeViewModel = new HomeViewModel(categories, MapperConfig.Mapper.ContestItemsDtoToContestItemsVM(contestItems), cnt);

                return View(homeViewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorInfo(ex, "Home", "Index"));
            }
        }

    }
}
