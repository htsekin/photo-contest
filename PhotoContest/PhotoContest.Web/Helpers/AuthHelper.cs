﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Contracts;
using PhotoContest.Web.MapperConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUserService userService;
        private readonly IOrganizerService organizerService;

        public AuthHelper(IUserService userService, IOrganizerService organizerService)
        {
            this.userService = userService;
            this.organizerService = organizerService;
        }

        public async Task<UserDTO> GetUserByUsername(string username)
        {
            var user = await this.userService.GetAsync(username);

            var userDTO = Mapper.UserToUserDTO(user);

            return userDTO;
        }

        public async Task<OrganaizerDTO> GetOrganizerByUsername(string username)
        {
            return Mapper.OrganizerToOrganizerDTO(await this.organizerService.GetByUsernameAsync(username));
        }

        public static string GetCurrentUserType(HttpContext httpContext)
        {
            if (httpContext.Session.GetString("junkieUsername") != null)
                return "User";

            if (httpContext.Session.GetString("organizerUsername") != null)
                return "Organaizer";

            return "Unknown";
        }
    }
}
