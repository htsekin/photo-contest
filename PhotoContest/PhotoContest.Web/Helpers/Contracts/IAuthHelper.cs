﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.Helpers.Contracts
{
    public interface IAuthHelper
    {
        Task<UserDTO> GetUserByUsername(string username);
        Task<OrganaizerDTO> GetOrganizerByUsername(string username);
    }
}
