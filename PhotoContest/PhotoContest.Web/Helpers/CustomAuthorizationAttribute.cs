﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.Helpers
{
    public class CustomAuthorizationAttribute
    {
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
        public class CustomAuthorizeAttribute : Attribute, IAuthorizationFilter
        {
            public void OnAuthorization(AuthorizationFilterContext context)
            {
                var junkieSession = context.HttpContext.Session.GetString("junkieUsername");
                var organizerSession = context.HttpContext.Session.GetString("organizerUsername");

                if (junkieSession == null && organizerSession == null)
                    context.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new { controller = "Account", action = "Login"}));
            }
        }
    }
}
