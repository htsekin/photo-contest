﻿using PhotoContest.Database.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Models;
using System.Collections.Generic;

namespace PhotoContest.Web.MapperConfig
{
    public static class Mapper
    {
        public static List<CategoryDTO> ListCategoryToListDTO(List<Category> categories)
        {
            var categoriesDTO = new List<CategoryDTO>();
            foreach (var category in categories)
            {
                categoriesDTO.Add(new CategoryDTO(category));
            }

            return categoriesDTO;
        }

        public static List<ContestItemDTO> ListContestItemToListDTO(List<ContestItem> contestItems)
        {
            var contestItemsDTO = new List<ContestItemDTO>();
            foreach (var contestItem in contestItems)
            {
                contestItemsDTO.Add(new ContestItemDTO(contestItem));
            }

            return contestItemsDTO;
        }

        public static List<ContestDTO> ListContestToListDTO(List<Contest> contests)
        {
            var contestDTO = new List<ContestDTO>();
            foreach (var contest in contests)
            {
                contestDTO.Add(new ContestDTO(contest));
            }

            return contestDTO;
        }

        public static UserDTO UserToUserDTO(User user)
        {
            if (user == null)
            {
                return null;
            }

            var result = new UserDTO(user);

            return result;
        }

        public static OrganaizerDTO OrganizerToOrganizerDTO(Organizer organizer)
        {
            if (organizer == null)
            {
                return null;
            }

            var result = new OrganaizerDTO(organizer);

            return result;
        }

        public static List<ContestItemDTO> ContestItemToContestItemDTO(List<ContestItem> contestItems)
        {
            var contestItemsDTO = new List<ContestItemDTO>();

            foreach (var item in contestItems)
            {
                contestItemsDTO.Add(new ContestItemDTO(item));
            }
            return contestItemsDTO;
        }

        public static WinnerViewModel ContestItemDTOToWinnerVM(ContestItemDTO contestItem)
        {
            var winner = new WinnerViewModel();

            winner.Title = contestItem.Title;
            winner.Score = contestItem.Score;
            winner.UserName = contestItem.Name;
            winner.RankingPoints = contestItem.RankingPoints;

            return winner;
        }

        public static List<WinnerViewModel> ContestItemsDTOToWinnersVM(List<ContestItemDTO> contestItems)
        {
            var winners = new List<WinnerViewModel>();

            foreach (var item in contestItems)
            {
                winners.Add(MapperConfig.Mapper.ContestItemDTOToWinnerVM(item));
            }
            return winners;
        }

        public static ContestItemViewModel ContestItemDtoToContestItemVM(ContestItemDTO contestItem)
        {
            var result = new ContestItemViewModel();
            
            result.ContestItemId = contestItem.ContestItemId;
            result.ContestId = contestItem.ContestId;
            result.Name = contestItem.Name;
            result.Title = contestItem.Title;
            result.Story = contestItem.Story;
            result.Score = contestItem.Score;
            result.PhotoName = contestItem.PhotoName;
            
            foreach (var item in contestItem.Reviews)
            {
                result.Reviews.Add(new ReviewViewModel
                {
                    ReviewId = item.ReviewId,
                    Score = item.Score,
                    Comment = item.Comment,
                    ReviewerUser = item.UserName,
                    ReviewerOrganizer = item.OrganizerName
                });
            }

            return result;
        }

        
        public static List<ContestItemViewModel> ContestItemsDtoToContestItemsVM(List<ContestItemDTO> contestItems)
        {
            var contestItemsVM = new List<ContestItemViewModel>();
            foreach (var contest in contestItems)
            {
                contestItemsVM.Add(MapperConfig.Mapper.ContestItemDtoToContestItemVM(contest));
            }

            return contestItemsVM;
        }

        public static List<CategoryViewModel> CategoriesDtoToCategoriesVM(List<CategoryDTO> categories)
        {
            var categoriesVM = new List<CategoryViewModel>();
            foreach (var category in categories)
            {
                categoriesVM.Add(new CategoryViewModel(category));
            }

            return categoriesVM;
        }


        public static UserViewModel UserDtoToUserVM(UserDTO user, string rank = null)
        {
            var userVM = new UserViewModel();
            userVM.UserId = user.UserId;
            userVM.FirsName = user.FirsName;
            userVM.LastName = user.LastName;
            userVM.Username = user.Username;
            userVM.RankingPoints = user.RankingPoints;
            userVM.Rank = rank;

            return userVM;
        }

        public static List<UserViewModel> UsersDtoToUsersVM(List<UserDTO> users)
        {
            var usersVM = new List<UserViewModel>();
            foreach (var user in users)
            {
                usersVM.Add(MapperConfig.Mapper.UserDtoToUserVM(user));
            }

            return usersVM;
        }

        public static OrganizerViewModel OrganizerDtoToOrganizerVM(OrganaizerDTO organizer)
        {
            var organizerVM = new OrganizerViewModel();
            organizerVM.FirsName = organizer.FirsName;
            organizerVM.LastName = organizer.LastName;
            organizerVM.Username = organizer.Username;

            return organizerVM;
        }

    }
}
