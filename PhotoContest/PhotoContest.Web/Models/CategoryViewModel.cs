﻿using PhotoContest.Services.DTOs;

namespace PhotoContest.Web.Models
{
    public class CategoryViewModel
    {
        public CategoryViewModel() { }
        public CategoryViewModel(CategoryDTO category)
        {
            this.CategoryId = category.CategoryId;
            this.CategoryName = category.CategoryName;
        }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
