﻿using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class ContestItemViewModel
    {
        public int ContestItemId { get; set; }
        public string Name { get; set; }
        public int ContestId { get; set; }
        public string Title { get; set; }
        public string Story { get; set; }
        public double Score { get; set; }
        public string PhotoName { get; set; }
        public List<ReviewViewModel> Reviews { get; set; } = new List<ReviewViewModel>();

    }
}
