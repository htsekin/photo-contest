﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class ContestViewModel
    {
        public int ContestId { get; set; }
        public bool IsAbleToEnroll { get; set; }
        public bool IsCurrentUserContestJury { get; set; }
        public string Title { get; set; }
        public string Photo { get; set; }
        public int CurrentUserRaitingPoints { get; set; }
        public string TimeToNextPhase { get; set; }
        public string Category { get; set; }
        public string ContestType { get; set; }
        public string CurrentPhase { get; set; }
        public string TimeUntilNextPhase { get; set; }
        public double Score { get; set; }
        public bool IsEnrolled { get; set; }
        public bool IsInvited { get; set; }
        public List<ContestItemViewModel> ContestItems { get; set; } = new List<ContestItemViewModel>();
        public List<WinnerViewModel> ContestWiners { get; set; } = new List<WinnerViewModel>();
        public PhaseOneViewModel PhaseOneViewModel { get; set; }
        public PhaseTwoViewModel PhaseTwoViewModel { get; set; }
    }
}
