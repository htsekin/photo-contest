﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class ContestsViewModel
    {
        public List<ContestDTO> Contests { get; set; }
        public string SelectedType { get; set; }
    }
}
