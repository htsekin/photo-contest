﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models
{
    public class CreateContestViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        [BindProperty]
        public int Category { get; set; }
        public List<SelectListItem> Categories { get; set; }
        [Required]
        [BindProperty]
        public int Type { get; set; }

        public List<SelectListItem> Types { get; set; }

        [Required]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [BindProperty]
        public List<int> Participants { get; set; }
        public List<SelectListItem> ParticipantsAsSelList { get; set; }

        [DisplayName("Phase One")]
        [Range(1, 31)]
        [Required]
        public int PhaseOne { get; set; }
        [DisplayName("Phase Two")]
        [Range(1, 24)]
        [Required]
        public int PhaseTwo { get; set; }
        [Required]
        [BindProperty]
        public List<int> Juries { get; set; }
        public List<SelectListItem> JuriesAsSelList { get; set; }
        public IFormFile Image { get; set; }
    }
}
