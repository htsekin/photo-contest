﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class DashboardJunkieViewModel
    {
        public UserDTO User { get; set; }
        public string MyRankingInfo { get; set; }
        public List<ContestDTO> OpenContests { get; set; } = new List<ContestDTO>();
        public bool IsEnrollPossible { get; set; }
        public List<ContestDTO> MyRunningContests { get; set; } = new List<ContestDTO>();
        public List<ContestDTO> MyFinishedContests { get; set; } = new List<ContestDTO>();
        public List<ContestDTO> ContestsInvitedIn { get; set; } = new List<ContestDTO>();
        public List<ContestDTO> ContestsJuryIn { get; set; } = new List<ContestDTO>();

    }
}
