﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class DashboardOrganaizerViewModel
    {
        public List<ContestDTO> ContestPhaseOne { get; set; } = new List<ContestDTO>();
        public List<ContestDTO> ContestPhaseTwo { get; set; } = new List<ContestDTO>();
        public List<ContestDTO> ContestPhaseFinished { get; set; } = new List<ContestDTO>();
        public List<UserViewModel> Users { get; set; } = new List<UserViewModel>();
        public OrganizerViewModel Organaizer { get; set; }
    }
}
