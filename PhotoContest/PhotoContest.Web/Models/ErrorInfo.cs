﻿using System;

namespace PhotoContest.Web.Models
{
    public class ErrorInfo
    {
        public ErrorInfo(Exception exception, string controllerName, string actionName)
        {
            this.Exception = exception;
            this.ControllerName = controllerName;
            this.ActionName = actionName;
        }

        public Exception Exception { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
}
