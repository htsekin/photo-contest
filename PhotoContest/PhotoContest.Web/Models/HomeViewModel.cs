﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;

namespace PhotoContest.Web.Models
{
    public class HomeViewModel
    {
        public HomeViewModel() { }
        public HomeViewModel(List<CategoryDTO> categories, List<ContestItemViewModel> contestItem, int openContestForEnroll)
        {
            this.Categories = MapperConfig.Mapper.CategoriesDtoToCategoriesVM(categories);
            this.ContestItems = contestItem;
            this.OpenContestForEnroll = openContestForEnroll;
        }
        public List<CategoryViewModel> Categories { get; set; } = new List<CategoryViewModel>();
        public List<ContestItemViewModel> ContestItems { get; set; } = new List<ContestItemViewModel>();
        public int OpenContestForEnroll { get; set; }
    }
}
