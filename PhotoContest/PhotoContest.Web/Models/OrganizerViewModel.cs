﻿namespace PhotoContest.Web.Models
{
    public class OrganizerViewModel
    {
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
    }
}
