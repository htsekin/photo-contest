﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Helpers.Strings;
using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models
{
    public class PhaseOneViewModel
    {

        public int ContestId { get; set; }
        public List<ContestItemViewModel> ContestItems { get; set; } = new List<ContestItemViewModel>();
        [Required]
        [StringLength(100, ErrorMessage = ErrorMessagesTypes.ContestItemTitleFailValidation, MinimumLength = 2)]
        public string Title { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = ErrorMessagesTypes.ContestItemStoryFailValidation, MinimumLength = 2)]
        public string Story { get; set; }

        [Required(ErrorMessage = ErrorMessagesTypes.UploadImageValidation)]
        public IFormFile Photo { get; set; }
    }
}
