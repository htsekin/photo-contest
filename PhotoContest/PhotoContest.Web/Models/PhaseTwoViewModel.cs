﻿using PhotoContest.Services.DTOs;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models
{
    public class PhaseTwoViewModel
    {
        [Range(minimum: 1, maximum: 10)]
        public int Score { get; set; }
        [Required]
        public string Comment { get; set; }
        [Required]
        [DisplayName("Wrong Category?")]
        public bool IsInAppropriateCat { get; set; }
        public List<ContestItemViewModel> ContestItems { get; set; } = new List<ContestItemViewModel>();
        public int ContestId { get; set; }
        public int ContestItemId { get; set; }
    }
}
