﻿using PhotoContest.Services.DTOs;

namespace PhotoContest.Web.Models
{
    public class ReviewViewModel
    {
        public int ReviewId { get; set; }
        public double Score { get; set; }
        public string Comment { get; set; }
        public string ReviewerUser { get; set; }
        public string ReviewerOrganizer { get; set; }
    }
}