﻿namespace PhotoContest.Web.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RankingPoints { get; set; }
        public string Rank { get; set; }
    }
}
