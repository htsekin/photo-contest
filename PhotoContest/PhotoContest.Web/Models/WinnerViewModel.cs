﻿namespace PhotoContest.Web.Models
{
    public class WinnerViewModel
    {
        public string Title { get; set; }
        public double Score { get; set; }
        public string UserName { get; set; }
        public int RankingPoints { get; set; }
    }
}
