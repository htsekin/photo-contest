using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PhotoContest.Database;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Helpers;
using PhotoContest.Web.Helpers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            services.AddDbContext<PhotoDbContext>(options =>
                options.UseSqlServer(@"Server=.\SQLEXPRESS;Database=PhotoContextDB;Integrated Security=True"
            ));

            services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            
            services.AddScoped<IContestService, ContestService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IContestTypeService, ContestTypeService>();
            services.AddScoped<IContestItemService, ContestItemService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IOrganizerService, OrganaizerService>();
            services.AddScoped<IRankService, RankService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IContestJuryService, ContestJuryService>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            services.AddScoped<IInvitedUsers, InvitedUsers>();

            services.AddSession();

            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Photo Contest");
            });

            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapFallback(context => {
                    context.Response.Redirect("/Error/NotFound");
                    return Task.CompletedTask;
                });

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
