[![Swagger](https://img.shields.io/badge/swagger-valid-brightgreen)]()


# Photo Contest

![80x80](https://gitlab.com/htsekin/photo-contest/-/raw/master/PhotoContest/PhotoContest.Web/wwwroot/images/logo.PNG)

<br/><br/>
Photo Contests site will help you discover competitions, contests, internships & opportunities. Get Instant Feedback on your photos. A team of aspiring photographers want an application that can allow them to easily manage online photo contests.

**[Click](https://gitlab.com/htsekin/photo-contest/-/boards/2734571) to view a board.**

***The project was created by [Hristo Tsekin](https://gitlab.com/htsekin) and  [Cvetomir Georgiev](https://gitlab.com/flowerpeac3)***

[[_TOC_]]


## How to Run the Project

1. Run SQLEXPRESS
2. Run solution: "PhotoContest.sln"
3. Check connection string in PhotoContest.Web => startup.cs
*   options.UseSqlServer(@"Server=.\SQLEXPRESS;Database=PhotoContextDB;Integrated Security=True")

4. If folder "Migration" is empty add migration (if folder is not empty go to 4.3) from 
* 	Package Manager Console:
* 	4.1 before start typing check "Defaul project to be: PhotoContest.Database".
* 	4.2 type command in PM> "add-migration Initial" (PM> add-migration Initial).
* 	4.3 next command is to create Database "PhotoContextDB" on your Database Server SQLEXPRESS
* 	  PM> "update-database" (PM> update-database). 

5. Select Start Project in Visual Studio -> Solution Explorer -> 
* 	right click on "PhotoContest.Web" and choose "Set as Startup Project"

## Structure and content

 1. Take part in photo contest with your images
 2. Juries can review all images and rate them
 3. See all contest by its phase
 4. You can see all categories
 5. Top 10 images can be seen ot home page
 6. When contest is in finish phase winners can be seen
 7. Can register and login in your private account
 8. There is two types of contests - invitational and open
 9. User can become a jury if collect enough points
 10. User can upload images 
 
## Main advantages discovered

>Simple frontend design\
Follow OOP’s best practices and the SOLID principles\
Implement a security mechanism for managing users and organizers\
The application has appropriate customer and server validations\
Follow the best practices when designing a REST API\
Use ASP.NET Core MVC with Razor as the template engine and bootstrap - CSS framework\
The API is documented with swagger

##  Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software 

 - [Visual Studio 2019](https://visualstudio.microsoft.com/vs/) 
 - [Entity Framework](https://www.nuget.org/packages/EntityFramework/)
 - [SQL Server](https://www.microsoft.com/en-us/sql-server/)

## Used Technologies

- ASP.NET Core 3.1
- Entity Framework Core
- MS SQL Server
- HTML
- CSS
- jQuery
- Bootstrap

## Database

![Database](https://gitlab.com/htsekin/photo-contest/-/raw/master/DatabaseSchemaImageV4.png)



## Public Part 

Accessible content that doesn’t require registration. 
Show the latest winning photos, available categories and count 
of contests that are in Phase I

Users that are not logged-in:
>Home page\
>Contests without details\
>Gallery\
>Login or registration form


## User Registration Process

To register, anonymous users must provide their username, first name, last name and their password.


## Organizer

Organizers can administer all major information objects in the system. The administrators have the following capabilities:

>Create contests\
>Rate all photos\
>View all contest their photos and all reviews after Phase I\
> Can select junkies that can be juries from a list\
> To choose contest type, and if Invitational to select participants

## Junky

>Junkies have a private area in the web application accessible after successful login, they could see all their contests and reviews for photos that are upload. 
>Can choose from different categories. 
>On their Dashboard can see: all open contests, contests that are jury in them, invited in contests, contests that are running (Phase I) and a list with all finished contests in which they can view reviews and winners.

## Jury

>In addition to junkie capabilities, junkie can become a jury after reach certain rating.
>When become a Jury he can be selected by Organizer to be jury in contests, that way he can write reviews (rate photos and leave comments).

## REST API

## Open source software to collaborate on code

- Create contest
- Register new user
- Create contest item, if contest is in phase I
- Organizer or User with rank jury can create reviews


## Screeshots

# Registration Form
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/Registration.PNG)


# Login Form
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/login.PNG)


# Contests
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/contests-page.PNG)


# Create Contest - Invitational
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/invitational-createcontest-page.PNG)


# Create Contest - Open
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/open-createcontest-page.PNG)


# Create Contest - Add file
![](https://gitlab.com/htsekin/photo-contest/-/raw/master/screenshots/file-upload.PNG)
